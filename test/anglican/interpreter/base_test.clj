(ns anglican.interpreter.base-test
  (:refer-clojure :exclude [eval apply])
  (:use anglican.interpreter.base)
  (:require [clojure.core :as core])
  (:require [clojure.java.io :as io])
  (:require [anglican.math :as math])
  (:use clojure.test))

(set! *print-level* 3)

(deftest eval-test
  (testing "self-evaluating"
    (testing "boolean"
      ; true and false have to be self-evaluating
      (is (self-evaluating? true))
      (is (self-evaluating? false))
      ; eval on a string must return the string
      (is (= true (second (eval empty-state 'true))))
      (is (= false (second (eval empty-state 'false))))
    )
    (testing "strings"
      ; strings have to be self-evaluating
      (is (self-evaluating? "string"))
      ; eval on a string must return the string
      (is (= "string"  (second (eval empty-state '"string"))))
    )
    (testing "numbers"
      ; numbers have to be self-evaluating
      (is (self-evaluating? 1))
      (is (self-evaluating? -1))
      (is (self-evaluating? -1/2))
      (is (self-evaluating? -1.2))
      ; eval on a number must return the number
      (is (= 1 (second (eval empty-state '1))))
      (is (= -1 (second (eval empty-state '-1))))
      (is (= -1/2 (second (eval empty-state '-1/2))))
      (is (= -1.2 (second (eval empty-state '-1.2))))
    )
    (testing "empty list"
      (is (= '() (second (eval empty-state '()))))
    )
  )
  (testing "procedures"
    (testing "primitive"
      ; every object in primitives must be a function
      (is (every? ifn? primitives))

      (testing "relational"
        ; tests for =, >, <, >=, <=
        (is (= true (second (eval empty-state '(not= 1 0)))))
        (is (= true (second (eval empty-state '(= 1 1)))))
        (is (= true (second(eval empty-state '(> 1 0)))))
        (is (= true (second(eval empty-state '(< 0 1)))))
        (is (= true (second(eval empty-state '(>= 1 1)))))
        (is (= true (second(eval empty-state '(<= 1 1)))))
        (is (= true (second(eval empty-state '(>= 1 0)))))
        (is (= true (second(eval empty-state '(<= 0 1)))))
      )
      (testing "arithmetic"
        ; tests for + - * /
        (is (= 1 (second (eval empty-state '(+ 1 0)))))
        (is (= 1 (second (eval empty-state '(+ 0 1)))))
        (is (= 3 (second (eval empty-state '(+ 1 2)))))
        (is (= 3 (second (eval empty-state '(+ 2 1)))))
        (is (= 1.0 (second (eval empty-state '(+ 0 1.0)))))
        (is (= 1.0 (second (eval empty-state '(+ 1.0 0)))))
        (is (= 1.0 (second (eval empty-state '(+ 0.5 1/2)))))
        (is (= 1.0 (second (eval empty-state '(+ 1/2 0.5)))))
        (is (= 1 (second (eval empty-state '(+ 1/2 1/2)))))
        (is (= 1 (second (eval empty-state '(- 1 0)))))
        (is (= -1 (second (eval empty-state '(- 0 1)))))
        (is (= 1 (second (eval empty-state '(- 2 1)))))
        (is (= 1.0 (second (eval empty-state '(- 1 0.0)))))
        (is (= 1.0 (second (eval empty-state '(- 1.0 0)))))
        (is (= 2 (second (eval empty-state '(* 2 1)))))
        (is (= 2 (second (eval empty-state '(* 1 2)))))
        (is (= 42 (second (eval empty-state '(* 6 7)))))
        (is (= 1/42 (second (eval empty-state '(* 1/6 1/7)))))
        (is (= 3.0 (second (eval empty-state '(* 0.25 12)))))
        (is (= 3.0 (second (eval empty-state '(* 1/4 12.0)))))
        ; FAILS (but should not)
        (is (= 2 (second (eval empty-state '(/ 2 1)))))
        ; FAILS (but should not)
        (is (= 1/2 (second (eval empty-state '(/ 1 2)))))
        (is (= 4.0 (second (eval empty-state '(/ 1 0.25)))))

        ; tests for mod
        (is (= 1 (second (eval empty-state '(mod 5 2)))))
        (is (= 1 (second (eval empty-state '(mod -5 2)))))
        (is (= -1 (second (eval empty-state '(mod 5 -2)))))
        (is (= -1 (second (eval empty-state '(mod -5 -2)))))
      )
      (testing "logic"
        ; tests for 'and' 'or'
        (is (= true (second (eval empty-state '(and)))))
        (is (= true (second (eval empty-state '(and true true)))))
        (is (= true (second (eval empty-state '(and true true true)))))
        (is (= true (second (eval empty-state '(or false true)))))
        (is (= true (second (eval empty-state '(or true false)))))
        (is (= true (second (eval empty-state '(or false false true)))))
        (is (= false (second (eval empty-state '(or)))))
        (is (= false (second (eval empty-state '(and true false)))))
        (is (= false (second (eval empty-state '(and false true)))))
        (is (= false (second (eval empty-state '(and true true false)))))
        (is (= false (second (eval empty-state '(or false false)))))
        (is (= false (second (eval empty-state '(or false false false)))))
      )
      (testing "lists"
        (is (every? true?
                    (map = '(1 2 3)
                           (second (eval empty-state '(list 1 2 3))))))
        (is (every? true?
                    (map = '(2 3)
                           (second (eval empty-state '(rest (list 1 2 3)))))))
        (is (= 1
               (second (eval empty-state '(first (list 1 2 3))))))
        (is (= 2
               (second (eval empty-state '(second (list 1 2 3))))))
        (is (= 3
               (second (eval empty-state '(nth (list 1 2 3) 2)))))
        (is (= 3
               (second (eval empty-state '(count (list 1 2 3))))))
        (is (= (set [1 2 3])
               (set (second (eval empty-state '(unique (list 1 2 2 3))))))))
      (testing "lambda"
        (is (= 1 (second (eval empty-state
                               '((lambda () 1))))) )
        (is (= 2 (second (eval empty-state
                               '((lambda (x) (+ x 1)) 1)))))
        (is (= 3 (second (eval empty-state
                               '((lambda (x y) (+ x y)) 1 2)))))
        (is (= 3 (second (eval empty-state
                               '((lambda args (+ (first args) (second args))) 1 2)))))
        (is (= 1 (second (eval empty-state
                               '(apply (lambda () 1) (list))))))
        (is (= 1 (second (eval empty-state
                               '(apply (lambda () 1))))))
        (is (= 2 (second (eval empty-state
                               '(apply (lambda (x) (+ x 1)) (list 1))))))
        (is (= 3 (second (eval empty-state
                               '(apply (lambda (x y) (+ x y)) (list 1 2))))))
        (is (= 3 (second (eval empty-state
                               '(apply (lambda args
                                         (+ (first args) (second args)))
                                       (quote (1 2)))))))
      )
    )
  )
  (testing "define"
    ; define variable and alias
    (let [[state _] (eval empty-state '(define x 1))
          [state _] (eval state '(define y x))]
      (is (= 1 (second (eval state 'x))))
      (is (= 1 (second (eval state 'y)))))
    ; define function
    (let [[state _] (eval empty-state
                          '(define f
                             (lambda (x y)
                               (+ x y))))]
      (is (= 3 (second (eval state '(f 1 2))))))
    ; binding to reserved words should throw an error
    (is (thrown? RuntimeException
                 (eval empty-state '(define define 1))))
    (is (thrown? RuntimeException
                 (eval empty-state '(define quote 1))))
    (is (thrown? RuntimeException
                 (eval empty-state '(define quote 1))))
    (is (thrown? RuntimeException
                 (eval empty-state '(define lambda 1))))
    (is (thrown? RuntimeException
                 (eval empty-state '(define if 1))))
    (is (thrown? RuntimeException
                 (eval empty-state '(define cond 1))))
    (is (thrown? RuntimeException
                 (eval empty-state '(define begin 1))))
    ; ; defining the output of a define should throw an error
    ; ; this currently fails, but should not
    ; (is (thrown? RuntimeException
    ;              (eval empty-state '(define y (define x 1)))))
  )
  (testing "let"
    (is (= 1 (second (eval empty-state '(let () 1)))))
    (is (= 1 (second (eval empty-state '(let ((a 0) (b 1)) (+ a b))))))
    (is (= 0 (second (eval empty-state '(let ((a 0) (b 1)) (+ a b) 0))))))
  (testing "control-logic"
    (is (= 1 (second (eval empty-state '(if true 1 2)))))
    (is (= 2 (second (eval empty-state '(if false 1 2)))))
    (is (= 1 (second (eval empty-state '(if (< 1 2) 1 2)))))
    (is (= 2 (second (eval empty-state '(if (> 1 2) 1 2)))))
    (is (= 1 (second (eval empty-state '(cond ((< 1 2) 1)
                                             ((> 1 2) 2)
                                             (else 3))))))
    (is (= 2 (second (eval empty-state '(cond ((> 1 2) 1)
                                             ((< 1 2) 2)
                                             (else 3))))))
    (is (= 3 (second (eval empty-state '(cond ((= 1 2) 1)
                                             ((= 1 2) 2)
                                             (else 3))))))
    (is (= 4 (second (eval empty-state '(begin
                                         (define x 2)
                                         (* x x ))))))
  )
  (testing "explicit eval"
    (is (= 3 (second (eval empty-state '(eval (+ 1 2))))))
    (is (= 3 (second (eval empty-state '(eval (quote (+ 1 2)))))))
    (is (= '(+ 1 2) (second (eval empty-state '(eval (quote (quote (+ 1 2))))))))
  )
)

(deftest basic-functions-test
  (let [[state _] (eval empty-state '(define f
                                      (lambda (n)
                                        (define a 1)
                                        (define b 2)
                                        (+ n (+ a b)))))]
    (is (= 6 (second (eval state '(f 3))))))
  (let [[state _] (eval empty-state '(define fib
                           (lambda (n)
                             (cond ((= n 0) 1)
                                   ((= n 1) 1)
                                   (else (+ (fib (- n 1)) (fib (- n 2))))))))]
    (is (= 144 (second (eval state '(fib 11))))))
)

(deftest memoized-fuctions-test
  (let [[state symb] (eval empty-state '(define mfib
                                         (mem
                                           (lambda (n)
                                             (cond ((= n 0) 1)
                                                   ((= n 1) 1)
                                                   (else (+ (mfib (- n 1))
                                                            (mfib (- n 2)))))))))
        [state val] (eval state '(mfib 10))
        addr (:addr (get-var state 'mfib))]
    (= 11 (count (get-in state [:proc-state addr])))))

;(deftest diagnostic-procedures
;  (def env (setup-environment))
;  ; test run time counter
;  (reset-start-time!)
;  (def t @start-time)
;  (is (> t (eval empty-state '(time))))
;  ; ensure last-log-prob initialized to nil
;  (reset-last-log-prob!)
;  (is (nil? @last-log-prob))
;  ; test eval and apply count
;  (reset-all-counters!)
;  (eval empty-state '(+ 1 1))
;  (is (= 4 (@counters :eval)))
;  (is (= 1 (@counters :apply)))
;  ; test globals and locals
;  ; erps
;  (is (= (set (keys primitive-elementary-random-procedures))
;         (set (eval empty-state '(locals "erp")))))
;  (is (= (set (keys primitive-elementary-random-procedures))
;         (set (eval empty-state '(globals "erp")))))
;  (is (clojure.set/subset? (set (keys primitive-elementary-random-procedures))
;                           (set (eval empty-state '(locals)))))
;  (is (clojure.set/subset? (set (keys primitive-elementary-random-procedures))
;                           (set (eval empty-state '(globals)))))
;  ; primitive procedures
;  (is (= (set (keys primitive-procedures))
;         (set (eval empty-state '(locals "primitive")))))
;  (is (= (set (keys primitive-procedures))
;         (set (eval empty-state '(globals "primitive")))))
;  (is (clojure.set/subset? (set (keys primitive-procedures))
;                           (set (eval empty-state '(locals)))))
;  (is (clojure.set/subset? (set (keys primitive-procedures))
;                           (set (eval empty-state '(locals)))))
;  (is (clojure.set/subset? (set (keys primitive-procedures))
;                           (set (eval empty-state '(globals)))))
;  ; compound procedures that must be defined
;  (def compound-procedures '(mem polya-urn-mem sum crp natural))
;  (is (clojure.set/subset? (set compound-procedures)
;                           (set (eval empty-state '(locals "compound")))))
;  (is (clojure.set/subset? (set compound-procedures)
;                           (set (eval empty-state '(globals "compound")))))
;  ; ensure known constanst in variables
;  (is (clojure.set/subset? (set (keys primitive-constants))
;                           (set (eval empty-state '(locals "variable")))))
;  (is (clojure.set/subset? (set (keys primitive-constants))
;                           (set (eval empty-state '(globals "variable")))))
;  ; ensure locals of newly created env is empty
;  (is (empty? (eval empty-state '(locals) (make-frame :parent))))
;  (is (empty? (eval empty-state '(locals "variable") (make-frame :parent))))
;  (is (empty? (eval empty-state '(locals "erp") (make-frame :parent))))
;  (is (empty? (eval empty-state '(locals "primitive") (make-frame :parent))))
;  (is (empty? (eval empty-state '(locals "compound") (make-frame :parent))))
;)

;(deftest erp-draws-cached
;  (binding [*cache-erp-draws* false
;            *rescore-erp-draws* false]
;    (def env (setup-environment))
;    ; test accumulation of log probability
;    (def first-sample (eval empty-state '(normal 1 1) env ['test-0]))
;    (def first-log-prob @last-log-prob)
;    (def second-sample (eval empty-state '(normal 1 1) env ['test-0]))
;    (def second-log-prob @last-log-prob)
;    (def third-sample (eval empty-state '(normal 2 2) env ['test-0]))
;    (def third-log-prob @last-log-prob)
;    (def fourth-sample (eval empty-state '(normal 2 2) env ['test-1]))
;    (def fourth-log-prob @last-log-prob)
;    ; samples at same address should be the different
;    (is (not= first-sample second-sample))
;    (is (not= first-log-prob second-log-prob))
;    ; samples at same address with different parameters should be different
;    (is (not= first-sample third-sample))
;    (is (not= first-log-prob third-log-prob))
;    ; a sample at a new address should be different
;    (is (not= first-sample fourth-sample))
;    (is (not= first-log-prob fourth-log-prob)))
;)

;(deftest erp-draws-cached
;  (binding [*cache-erp-draws* true
;            *rescore-erp-draws* false]
;    (def env (setup-environment))
;    ; test accumulation of log probability
;    (def first-sample (eval empty-state '(normal 1 1) env ['test-0]))
;    (def first-log-prob @last-log-prob)
;    (def second-sample (eval empty-state '(normal 1 1) env ['test-0]))
;    (def second-log-prob @last-log-prob)
;    (def third-sample (eval empty-state '(normal 2 2) env ['test-0]))
;    (def third-log-prob @last-log-prob)
;    (def fourth-sample (eval empty-state '(normal 2 2) env ['test-1]))
;    (def fourth-log-prob @last-log-prob)
;    ; samples at same address should be the same
;    (is (= first-sample second-sample))
;    (is (= first-log-prob second-log-prob))
;    ; samples at same address with different parameters should be different
;    (is (not= first-sample third-sample))
;    (is (not= first-log-prob third-log-prob))
;    ; a sample at a new address should be different
;    (is (not= first-sample fourth-sample))
;    (is (not= first-log-prob fourth-log-prob)))
;)

;(deftest erp-draws-cached-rescored
;  (binding [*cache-erp-draws* true
;            *rescore-erp-draws* true]
;    (def env (setup-environment))
;    ; test accumulation of log probability
;    (def first-sample (eval empty-state '(normal 1 1) env ['test-0]))
;    (def first-log-prob @last-log-prob)
;    (def second-sample (eval empty-state '(normal 1 1) env ['test-0]))
;    (def second-log-prob @last-log-prob)
;    (def third-sample (eval empty-state '(normal 2 2) env ['test-0]))
;    (def third-log-prob @last-log-prob)
;    (def fourth-sample (eval empty-state '(normal 2 2) env ['test-1]))
;    (def fourth-log-prob @last-log-prob)
;    ; samples at same address should be the same
;    (is (= first-sample second-sample))
;    (is (= first-log-prob second-log-prob))
;    ; samples at same address with different parameters should be rescored
;    (is (= first-sample third-sample))
;    (is (not= first-log-prob third-log-prob))
;    ; a sample at a new address should be different
;    (is (not= first-sample fourth-sample))
;    (is (not= first-log-prob fourth-log-prob)))
; )
