(ns anglican.transform-test
  (:require [clojure.test :refer [deftest testing is]])
  (:use anglican.transform))

(def tx-tag-it {:type :tag-it :tags :all})
(defmethod transform :tag-it [code tag tx] [[tag code] tx])

(deftest test-transform-events
  (letfn [(tag-it [code] (first (traverse code tx-tag-it)))]
    (testing "transform events"
      (is (= (tag-it 1) [:atom 1]) "atom")
      (is (= (tag-it 'x) [:reference 'x]) "reference")
      (is (= (tag-it '(quote (1)))) [:quote '(quote [:list ([:atom 1])])]) "list")
      (is (= (tag-it '(quote ())) [:quote '(quote [:list ()])]) "empty list")
      (is (= (tag-it '(max 1 2))
             [:application '([:reference max] [:atom 1] [:atom 2])]) "application")
      (is (= (tag-it '(lambda (x) y))
             [:lambda '(lambda ([:parameter x]) [:reference y])]) "lambda")
      (is (= (tag-it '(lambda () 1))
             [:lambda '(lambda () [:atom 1])]) "parameter-less lambda")
      (is (= (tag-it '(let ((x 1) (y 2)) (x y)))
             [:let '(let ([:binding ([:variable x] [:atom 1])]
                          [:binding ([:variable y] [:atom 2])])
                      [:application ([:reference x] [:reference y])])]) "let")
      (is (= (tag-it '(define a 1))
             [:define '(define [:variable a] [:atom 1])]) "define")
      (is (= (tag-it '(if x y z))
             [:if '(if [:reference x] [:reference y] [:reference z])]) "if3")
      (is (= (tag-it '(if w t))
             [:if '(if [:reference w] [:reference t])]) "if2")
      (is (= (tag-it '(cond (x) (y z) (else w)))
             [:cond '(cond [:cond-clause ([:reference x])]
                           [:cond-clause ([:reference y] [:reference z])]
                           [:cond-else (else [:reference w])])])
             "cond")
      (is (= (tag-it '(begin 1 x (sin 3.14)))
             [:begin '(begin [:atom 1] [:reference x]
                             [:application ([:reference sin] [:atom 3.14])])])
          "begin")
      (is (= (tag-it '(observe (normal 1 1) 1))
             [:observe '(observe [:distribution
                                  ((:reference normal) (:atom 1) (:atom 1))]
                                 (:atom 1))])
          "observe")))

(def tx-some-tags {:tags #{:list} :type :some-tags})
(defmethod transform :some-tags [code tag tx] [[tag code] tx])

(def tx-by-tag {:type :by-tag})
(defmethod transform [:atom :by-tag] [code tag tx] [[tag code] tx])
(defmethod transform [:application :by-tag] [code tag tx] [[tag code] tx])

(deftest test-dispatch
  (testing "dispatch"
    (is (= (first (traverse '(quote (1 2 (3) 4)) tx-some-tags))
           '(quote [:list (1 2 [:list (3)] 4)])) "some tags")
    (is (= (first (traverse '(map abs '(1 2 3)) tx-by-tag))
           '[:application (map abs '([:atom 1] [:atom 2] [:atom 3]))]) "by tag")))

(def tx-count-lists {:tags #{:list} :type :count-lists :count 0})
(defmethod transform :count-lists [code tag tx] [[tag code] (update-in tx [:count] inc)])

(def tx-number-applications {:type :number-applications :id 0})
(defmethod transform [:application :number-applications] [[operator & operands] tag tx]
  [(list* operator [:id (:id tx)] operands) (update-in tx [:id] inc)])

(def tx-sample {:type :sample})
(defmethod transform [:application :sample] [[operator & _ :as code] tag tx] 
  [(if (= operator 'normal) (list 'sample code) code) tx])

(deftest test-state-update
  (testing "state update"
    (is (= (:count (second (traverse '(quote (1 2 (3) 4)) tx-count-lists)))
           2) "count lists")
    (is (= (first (traverse '(if (positive x) x (abs x)) tx-number-applications))
           '(if (positive [:id 0] x) x (abs [:id 1] x))) "number applications")
    (is (= (first (traverse '(observe (normal 1 1) (normal 1 1)) tx-sample))
           '(observe (normal 1 1) (sample (normal 1 1))))
        "sample")))
