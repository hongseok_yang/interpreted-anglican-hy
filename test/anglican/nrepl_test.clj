(ns anglican.nrepl-test
  (:use clojure.test)
  (:require [slingshot.slingshot :refer [try+ throw+]])
  (:use anglican.nrepl))

(deftest test-block-queue
  (testing "block-queue"
    (binding [*block-size* 3]
      (let [bq empty-block-queue]
        (is (= (peek (conj bq 1)) [1])
            "single element")
        (is (= (peek (into bq [1 2 3 4])) [1 2 3])
            "only the first block is retrieved")
        (is (= (peek (pop (into bq [1 2 3 4 5]))) [4 5]) 
            "the second block contains the continuation")))))

(deftest test-nrepl-predict-queue
  (testing "predict queue"
    (start-inference {:sample-method "smc"})
    (is (= (do (push-predict! 'x 1) (retrieve-predicts)) [['x 1]]) "one in one one")
    (is (= (do (push-predict! 'x 1) (push-predict! 'y 2) (retrieve-predicts)) [['x 1] ['y 2]]) "right order")
    (is (= (do (retrieve-predicts)) []) "nothing")
    (stop-inference)))

(deftest test-nrepl-commands
  (letfn [(two-predicts []
            (start-inference {:sample-method "smc", :num-samples 2})
            (Thread/sleep 1000)
            (stop-inference)
            (retrieve-predicts))]

    (testing "end to end nrepl session"
      (is (= (do (clear-directives)
                 (add-directives "[predict 1]")
                 (two-predicts))
             [[1 1] [1 1]])
          "two predicts and that's it")
      (is (= (do (clear-directives)
                 (add-directives "[predict 3]")
                 (two-predicts))
             [[3 3] [3 3]])
          "clear directives")
      (is (= (do (add-directives "[predict 3]")
                 (two-predicts))
             [[3 3] [3 3] [3 3] [3 3]])))))

(deftest test-nrepl-errors
  (testing "nrepl errors"
    (clear-directives)
    (add-directives "[predict 0]")
    (is (do (start-inference {:sample-method "smc", :num-samples 1})
            (try+
              (start-inference {:sample-method "smc", :num-samples 1})
              (stop-inference)
              false
              (catch [:source :nrepl] _
                (stop-inference)
                true)))
        "nested inference not allowed")
    
    (is (do (start-inference {:sample-method "smc", :num-samples 1})
            (try+
              (add-directives "[predict 1]")
              (stop-inference)
              false
              (catch [:source :nrepl] _
                (stop-inference)
                true)))
        "directives cannot be added while inference is running")))
