(ns benchmark-pgibbs
  (:require [clojure.core.reducers :as r]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [clojure.java.io :as io]
            [anglican.parser :as parser]
            [anglican.math :as math]
            [anglican.sampler :refer [*predict* *map* *sample-count* *start-time* nanotime]]
            [anglican.sampler.pgibbs :as pgibbs] :reload-all))

(defn benchmark-pgibbs [src-file num-part num-sweep seed]
  (let [code (parser/venture-file src-file)]
    (math/reset-rng! seed)
    (pgibbs/run code num-part num-sweep)))

 (binding [*predict* (io/writer "benchmark-pgibbs-hwkot-map.out")
           *out* (io/writer "benchmark-pgibbs-hwkot-map.profile")
           *sample-count* 0
           *start-time* (nanotime)
           *map* pmap]
  (profile :info :Arithmetic
    (benchmark-pgibbs
      "resources/test-anglican-code/hmm-with-known-obs-and-trans.venture"
      10 10 1))
  nil)
