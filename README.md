# Anglican (Scheme/Venture Syntax)

This repository contains the original, interpreted, version of the probabilitic programming language Anglican. This version of Anglican uses a Scheme-like syntax that is (largely) interoperable with [Venture](http://probcomp.csail.mit.edu/venture/). For detailed documentation and tutorials visit the [Anglican Home Page](http://www.robots.ox.ac.uk/~fwood/anglican).

For the version of Anglican that is currently under active development, please see [Embedded Anglican](https://bitbucket.org/probprog/anglican). This version implements a domain specific language (DSL) within Clojure using [Continuation Passing Style](http://matt.might.net/articles/cps-conversion/) (CPS) transforms. The preferred syntax in Embedded Anglican is a subset of Clojure, although the legacy syntax is also still supported.


# Getting Started

The fastest and easiest way to download and install Anglican is:

1. Download the `anglican` shell script (Unix) or `jar` file (Windows) (see: [download instructions](http://www.robots.ox.ac.uk/~fwood/anglican/download/index.html))

2. Write a program code (see: [syntax documentation](http://www.robots.ox.ac.uk/~fwood/anglican/language/index.html))

3. Run your code with `anglican -s <filename.ang>` or `java -jar <anglican-version-master.jar> -s <filename.ang>` (see: [usage instructions](http://www.robots.ox.ac.uk/~fwood/anglican/usage/index.html))


# Installation from Source

In order to build a new `jar` file from source, do the following:

1. Install [java](https://java.com/en/download/), [leiningen](http://leiningen.org) and [maven](https://maven.apache.org) if needed

2. Clone this repository

3. In the source directory, execute the following commands

    ```
    mvn deploy:deploy-file -Dfile=resources/exterior_jars/umontreal-ssj-2.5.jar -DartifactId=umontreal-ssj -Dversion=2.5.0 -DgroupId=umontreal-ssj -Dpackaging=jar -Durl=file:maven
    mvn deploy:deploy-file -Dfile=resources/exterior_jars/forssj-optimization.jar -DartifactId=forssj-optimization -Dversion=1.0.0 -DgroupId=forssj-optimization -Dpackaging=jar -Durl=file:maven
    ```

4. Build a `jar` file by calling 

    ```
    lein uberjar
    ```

# License

Copyright ©, 2013, 2014, 2015 Wood group

This file is part of Anglican, a probabilistic programming system.

Anglican is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Anglican is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the [GNU General Public License](gpl-3.0.txt) along with Anglican.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

This work is supported under DARPA PPAML through the U.S. AFRL under Cooperative Agreement number FA8750-14-2-0004. The U.S. Government is authorized to reproduce and distribute reprints for Governmental purposes notwithstanding any copyright notation heron. The views and conclusions contained herein are those of the authors and should be not interpreted as necessarily representing the official policies or endorsements, either expressed or implied, of DARPA, the U.S. Air Force Research Laboratory or the U.S. Government.