(ns anglican.interpreter.ardb
  (:refer-clojure :exclude [eval apply])
  (:require [slingshot.slingshot :refer [throw+ try+]]

            [anglican.interpreter.base
             :as base
             :refer [Interpreter
                     seq-eval
                     eval apply score sample
                     expr-type eval-invalid apply-invalid inc-count]])

  (:import [anglican.interpreter.base
            ElementaryRandomProcedure ExchangeableRandomProcedure]))

(def ^:dynamic *rescore* "enable rescoring"
  true)

(declare eval-methods apply-methods
         apply-random-procedure
         get-cached assoc-cached assoc-sampled assoc-observed
         can-reuse? equal-dim?)

(defrecord ExecutionState
  [env-stack     ; [EnvironmentFrame ... ]
   proc-state    ; {addr {} ... }
   rand-state    ; {addr Sample ... } --- that is not true, apparently, there is some substructure
   call-stack    ; [addr ... ]
   eval-addr     ; addr
   system        ; no idea
   predicts      ; [predict ... ]
   adaptive      ; adaptive RDB state, defined in anglican.sampler.ardb
   ])

(defn apply-random-procedure
  "Application dispatch for random procedures. Looks up sample in cache
  and re-uses if the arguments are the same or rescoring is possible,
  or draws new sample."
  [state proc args]
  (let [cached (get-cached state)]
    (if (can-reuse? cached proc args)
      (if (= (:args cached) args)
        [(assoc-cached state cached) (:value cached)]          ; reuse cached sample
        (let [log-prob (score state proc args (:value cached)) ; rescore and reuse
              rescored (assoc cached :log-prob log-prob :args args)]
          [(assoc-cached state rescored) (:value rescored)]))
      (let [[state s] (sample state proc args)]                ; produce new sample
        [(assoc-sampled state s) (:value s)]))))

(defn get-cached
  "Get cached random choice if exists at the current address."
  [state]
  (let [addr (:eval-addr state)]
    (get-in state [:rand-state :cached addr])))

(defn add-logprobability
  "Accumulates spent/restored/observed logprobability to the current execution."
  [state key addition]
  (let
    [current-logprobability (get (:logprobabilities (:system state)) key)
     current-logprobability (if (nil? current-logprobability) 0 current-logprobability)
     sum (+ current-logprobability addition)]
    (assoc-in state [:system :logprobabilities key] sum)))

(defn logprob-from-sample
  "Get the logprobability of the random choice."
  [state s]
  (score state (:proc s) (:args s) (:value s)))

(defn assoc-cached
  "Put the random choice, which has been restored from the cached set,
  to the set of reused random choices. The logprobability will be
  either accumulated to the <<restored probability>> counter or
  artificially to the <<spent probability>> counter. The latter
  happens if we simulating the sampling of random choices in order to
  calculate Q(X_new -> X_old)."
  [state s]
  (let [addr (:eval-addr state)
        probability-key
          (if (and (contains? (:system state) :consider-as-new) (contains? (get-in state [:system :consider-as-new]) addr))
            :spent-probability
            :restored-probability)
        state (add-logprobability state probability-key (logprob-from-sample state s))
        state
          (if (= (type (:proc s)) ExchangeableRandomProcedure)
            (let
              [stats (get-in state [:proc-state (:addr (:proc s))])
               stats ((:assoc (:proc s)) stats (:value s))]
              (assoc-in state [:proc-state (:addr (:proc s))] stats))
            state)]
    (assoc-in state [:rand-state :reused addr] s)))

(defn assoc-sampled
  "Put the random choice to the set of sampled random choices,
  i.e. when the probability actually was spent."
  [state s]
  (let [addr (:eval-addr state)
        state (add-logprobability state :spent-probability (logprob-from-sample state s))]
    (assoc-in state [:rand-state :sampled addr] s)))

(defn assoc-observed
  "Put the random choice to the set of observed random choices."
  [state s]
  (let [addr (:eval-addr state)
        state (add-logprobability state :observed-probability (logprob-from-sample state s))]
    (assoc-in state [:rand-state :observed addr] s)))

(defn can-reuse?
  [s proc args]
  "Checks whether a sample can be reused given a new procedure and args"
  (and (some? s)
       (= (:proc s) proc)
       (or (= (:args s) args)
           (and *rescore* (equal-dim? (:args s) args)))))

(defn equal-dim?
  [a b]
  "Checks if a and b have same dimension. If a and b are collections,
  each element of a and b is checked recursively."
  (if (not (or (coll? a) (coll?  b)))
      true
      (if (and (coll? a) (coll? b) (= (count a) (count b)))
          (every? true? (map equal-dim? a b))
          false)))

;; the need to copy-and-paste this is a reason to think about redesign.

(defn eval-application
  "Evaluate function applications (proc arg arg ...)"
  [state expr]
  (let [[state proc-and-args] (seq-eval state expr 0)

        ;; propagate the meta-data to the prog-and-args
        ;; meta-data has to be propagated to operands because
        ;; of apply, consider: (apply apply (lambda () #t) '(()))
        ;; eval won't be supported; eval is evil anyway, but for
        ;; the purposes of ardb everything from within eval will
        ;; just go to a single arm, 0.

        [proc & args] (map (fn [o]
                             (if (instance? clojure.lang.IObj o)
                               (with-meta o (merge (meta o) (meta expr)))
                               o)) ; otherwise cannot be applied anyway
                           proc-and-args)]

    (try+
     (apply state proc (seq args))
     ; wrap most commonly occuring errors in something a little more descriptive
     (catch clojure.lang.ArityException _
       (throw+ {:source :anglican
                :type :wrong-arity
                :message (format "Wrong number of args (%d) passed to procedure %s"
                                 (count args)
                                 (:name proc))}))
     (catch java.lang.ClassCastException _
       (throw+ {:source :anglican
                :type :wrong-type
                :message (format "Type exception when trying to call procedure %s with arguments %s"
                                 (:name proc)
                                 (pr-str (seq args)))}))
     (catch java.lang.Exception _
       (throw+ {:source :anglican
                :type :unknown
                :message (format "Unidentified exception when trying to call procedure %s with arguments %s"
                                 (:name proc)
                                 (pr-str (seq args)))})))))

(def eval-methods
     (merge base/eval-methods
            {:application eval-application}))

(def apply-methods
     (merge base/apply-methods
            {ElementaryRandomProcedure apply-random-procedure
             ExchangeableRandomProcedure apply-random-procedure}))


(defn eval*
  "Dispatch function for evaluation of an expression. Determines
  expression type and calls the corresponding handler as defined
  in eval-methods."
  [state expr]
  (let [state (inc-count state :calls :eval)
        eval-fn (get eval-methods (expr-type expr) eval-invalid)]
   (eval-fn state expr)))

(defn apply*
  [state proc args]
  (let [state (inc-count state :calls :apply)
        apply-fn (get apply-methods (class proc) apply-invalid)]
    (apply-fn state proc args)))

(defn observe*
  [state expr value]
  (let [[state s] (base/observe* state expr value)]
    [(assoc-observed state s) s]))

(def intepreter-methods
  (assoc base/intepreter-methods
         :eval eval*
         :apply apply*
         :observe observe*))

(extend ExecutionState
  Interpreter
  intepreter-methods)

; empty execution state
(def empty-state
  (map->ExecutionState
   {:env-stack [base/global-env]
    :proc-state {}
    :rand-state {}
    :call-stack [[:global]]
    :eval-addr [:global]
    :system {}
    :predicts []
    :adaptive nil}))
