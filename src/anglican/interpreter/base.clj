(ns anglican.interpreter.base
  (:refer-clojure :exclude [eval apply time])
  (:require [clojure.core :as clj]
            [clojure.string :refer [join]]
            [anglican.math :as math]
            [anglican.math.erp :as erp]
            [anglican.math.xrp :as xrp]
            [clojure.core.matrix :as mat]
            [clojure.core.matrix.operators :as mop]
            [anglican.sampler :refer [Print to-str *start-time* time nanotime]]
            [slingshot.slingshot :refer [try+ throw+]]))

(defmacro dbg[x] `(let [x# ~x] (println "dbg:" '~x "=" x#) x#))

; language reserved words (may not be used as variable names)
(def directives
  #{'quote
    'and
    'or
    'define
    'lambda
    'if
    'cond
    'loop
    'let
    'begin
    'else
    'recur
    'system})

; An execution state contains
; {:proc-state {addr {} ...}
;  :env-stack [EnvironmentFrame ...]
;  :eval-addr  addr
;  :system {}}
(defrecord ExecutionState [proc-state env-stack eval-addr system])

; An environment frame contains
; {:bindings {[symbol expr] ...}
;  :parent   EnvironmentFrame}
(defrecord EnvironmentFrame [bindings parent])

; A sample contains
; {:value  value
;  :log-prob  double
;  :proc  ElementaryRandomProcedure or ExchangeableRandomProcedure
;  :args  arguments to procedure call}
(defrecord Sample [value log-prob proc args])

; This is used to signify a recur call from inside a compound procedure
(defrecord RecursiveApplication [args])

; A primitive procedure contains
; {:name symbol
;  :proc clojure.lang.IFn}
(defrecord PrimitiveProcedure [name proc])

(extend-type PrimitiveProcedure
  Print
  (to-str [proc] (str "primitive-procedure{"
                      ":name " (:name proc)
                      "}")))

; A compound procedure contains
; {:name symbol
;  :args list of symbols
;  :body list of anglican expressions
;  :addr address of environment relative to which procedure is defined}
(defrecord CompoundProcedure [name args body env])

(extend-type CompoundProcedure
  Print
  (to-str [proc] (str "compound-procedure{"
                      ":name " (:name proc) " "
                      ":args " (:args proc) " "
                      ":body " (:body proc)
                      "}")))

; A elementary random procedure contains
; {:name symbol
;  :rng  (fn [args&] ...) -> Sample
;  :log-pdf (fn [value args&]) -> double
(defrecord ElementaryRandomProcedure [name rng log-pdf])

(extend-type ElementaryRandomProcedure
  Print
  (to-str [proc] (str "elementary-random-procedure{"
                      ":name " (:name proc)
                      "}")))

; A core procedure contains
; {:name symbol
;  :proc (fn [state &args] ...) -> [state proc])}
(defrecord CoreProcedure [name proc])

(extend-type CoreProcedure
  Print
  (to-str [proc] (str "core-procedure{"
                      ":name " (:name proc)
                      "}")))

; An exchangeable random procedure contains
; {:name symbol
;  :rng  (fn [stats args&] ...) -> Sample
;  :log-pdf (fn [stats value args&] ...) -> double
;  :assoc (fn [stats value] ...) -> stats
;  :dissoc (fn [stats value] ...) -> stats}
;  :args contains arguments if instantiated
;  :addr eval addr if instantiated}
(defrecord ExchangeableRandomProcedure [name rng log-pdf assoc dissoc args addr])

(extend-type ExchangeableRandomProcedure
  Print
  (to-str [proc] (str "exchangeable-random-procedure{"
                      ":name " (:name proc)
                       "}")))

; A memoized procedure contains
; {:name symbol
;  :proc Procedure
;  :addr eval addr where procedure was instantiated}
(defrecord MemoizedProcedure [name proc addr])

(extend-type MemoizedProcedure
  Print
  (to-str [proc] (str "memoized-procedure{"
                      ":name " (:name proc) " "
                      ":proc " (to-str (:proc proc))
                      "}")))

; helper functions for eval
(declare eval-methods
         core-procs
         observe*
         eval*
         expr-type
         self-evaluating?
         procedure?
         sub-eval
         seq-eval
         eval-application
         eval-invalid
         eval-self-evaluating
         eval-symbol
         eval-quote
         eval-if
         eval-cond
         eval-define
         eval-begin
         parse-cond
         parse-loop
         parse-let
         init-compound-proc
         init-mem-proc)

; helper functions for apply
(declare apply-methods
         score-methods
         apply*
         apply-invalid
         apply-primitive
         apply-compound
         apply-memoized
         apply-erp
         init-xrp
         sample-xrp
         apply-xrp)

; helper functions for sample
(declare sample-methods
         sample-invalid
         sample-erp
         sample-xrp)

; helper functions for score
(declare score-methods
         score-invalid
         score-erp
         score-xrp)

; functions for managing execution state
(declare local-env
         get-var
         def-var
         reset-var!
         enter-frame
         exit-frame
         enter-expr
         exit-expr
         inc-count
         reset-count
         reset-time)

; basic interpreter protocol
(defprotocol Interpreter
  (eval [state expr])
  (apply [state proc args])
  (sample [state proc args])
  (score [state proc args value])
  (observe [state expr value]))

(defn eval*
  "Dispatch function for evaluation of an expression. Determines
  expression type and calls the corresponding handler as defined
  in eval-methods."
  [state expr]
  (let [state (inc-count state :calls :eval)
        eval-fn (get eval-methods (expr-type expr) eval-invalid)]
   (eval-fn state expr)))

(defn apply*
  "Dispatch function for apply. Determines procedure type and calls
  the corresponding handler as defined in apply-methods"
  [state proc args]
   (let [state (inc-count state :calls :apply)
         apply-fn (get apply-methods (class proc) apply-invalid)
         [state value] (apply-fn state proc args)]
     [state value]))

(defn sample*
  "Dispatch function for sample. Determines procedure type and calls
  the corresponding handler as defined in sample-methods"
  [state proc args]
  (let [state (inc-count state :calls :sample)
        sample-fn (get sample-methods (class proc) sample-invalid)]
   (sample-fn state proc args)))

(defn score*
  "Dispatch function for score. Determines the log-probability of
  the return value of a random procedure application"
  [state proc args value]
  (let [state (inc-count state :calls :score)
        score-fn (get score-methods (class proc) score-invalid)]
   (score-fn state proc args value)))


(defn observe*
  "Evaluates a random expression with constrained return value.
  Returns a pair [state s] containing an updated state and
  a Sample object."
  ([state expr value]
   (let [state (inc-count state :calls :observe)]
     (if (= (expr-type expr) :symbol)
       ; if expression is a symbol, lookup in enviroment and try again
       (recur state (get-var state expr) value)
       (if (not= (expr-type expr) :application)
         ; if expression is not an application then we can't observe
         (throw+
          {:source :anglican
           :type :invalid-observe
           :expr expr
           :message (str "Observed expression does not evaluate to a function application: '" expr)})
         ; evaluate procedure
         (let [[state operator] (sub-eval state (first expr) 0)
               [state args] (seq-eval state (rest expr) 2)]
           (if (= operator (core-procs 'eval))
             ; recur with unquoted expr on eval
             (let [[state expr] (eval state (first args))]
               (recur state expr value))
             ; all other applications must evaluate to something scoreable
             (let [[state value] (sub-eval state value 1)
                   proc (if (= operator (core-procs 'apply)) (first args) operator)
                   args (if (= operator (core-procs 'apply)) (second args) args)]
               (try+
                (let [lp (score state proc args value)
                      sample (Sample. value lp proc args)]
                  (if (instance? ExchangeableRandomProcedure proc)
                    ; if procedure is an xrp, then update state to incorporate the value
                    (let [ks [:proc-state (:addr proc)]
                          stats ((:assoc proc) (get-in state ks) value)]
                      [(assoc-in state ks stats) sample])
                    ; otherwise just return the state and sample
                    [state sample]))
                ; wrap most commonly occuring errors in something a little more descriptive
                (catch clojure.lang.ArityException _
                  (throw+ {:source :anglican
                           :type :wrong-arity
                           :message (format "Wrong number of args (%d) when observing %s"
                                            (count args)
                                            (:name proc))}))
                (catch java.lang.ClassCastException _
                  (throw+ {:source :anglican
                           :type :wrong-type
                           :message (format "Wrong argument type when trying to observe %s with arguments %s and value %s"
                                            (:name proc)
                                            (pr-str (seq args))
                                            (pr-str value))}))
                (catch java.lang.IllegalArgumentException _
                  (throw+ {:source :anglican
                           :type :wrong-type
                           :message (format "Argument type exception when trying to observe %s with arguments %s and value %s"
                                            (:name proc)
                                            (pr-str (seq args))
                                            (pr-str (seq value)))}))
                (catch Object _
                  (throw+ {:source :anglican
                           :type :unknown
                           :message (format "Unidentified exception when trying to observe procedure %s with arguments %s"
                                            (:name proc)
                                            (pr-str (seq args)))})))))))))))

(defn expr-type
  "Returns a keyword type for an expression. Used as a dispatch function by eval."
  [expr]
  ;(dbg expr)
  (cond
    (self-evaluating? expr) :self-evaluating
    (symbol? expr) :symbol
    (seq? expr)
      (let [operator (first expr)]
        (cond
         (contains? directives operator)
           (keyword (str operator))
         :else
           :application))
    ; this should never happen, in principle
    :else :invalid))

(defn eval-invalid
  "Handler for invalid expression type. If the parser does
  its job correctly, this should never execute."
  [state expr]
  (throw+ {:source :anglican
           :type :invalid-expression
           :expr expr
           :message (str "Expression is not a valid statement: '" expr)}))

(defn eval-application
  "Evaluate function applications (proc arg arg ...)"
  [state expr]
  (let [[state proc] (sub-eval state (first expr) 0)
        [state args] (seq-eval state (rest expr) 1)]
    (try+
     (apply state proc (seq args))
     ; wrap most commonly occuring errors in something a little more descriptive
     (catch clojure.lang.ArityException _
       (throw+ {:source :anglican
                :type :wrong-arity
                :message (format "Wrong number of args (%d) passed to procedure %s"
                                 (count args)
                                 (:name proc))}))
     (catch java.lang.ClassCastException _
       (throw+ {:source :anglican
                :type :wrong-type
                :message (format "Type exception when trying to call procedure %s with arguments %s"
                                 (:name proc)
                                 (pr-str (seq args)))}))
     (catch java.lang.Exception _
       (throw+ {:source :anglican
                :type :unknown
                :message (format "Unidentified exception when trying to call procedure %s with arguments %s"
                                 (:name proc)
                                 (pr-str (seq args)))})))))

(defn eval-recur
  "Evaluate recursive application (recur & args)"
  [state expr]
  (let [[state args] (seq-eval state (rest expr) 1)]
    [state (RecursiveApplication. args)]))

(defn eval-self-evaluating
  "Evaluate self-evaluating expressions: boolean, number, string, or empty list"
  [state expr]
  [state expr])

(defn eval-symbol
  "Evaluate symbols, results in lookup in environment"
  [state expr]
  [state (get-var state expr)])

(defn eval-quote
  "Evaluate quoted expression (quote expr)"
  [state expr]
  [state (second expr)])

(defn eval-and
  "Evaluates (and exprs&). Halts evaluation after first `false`."
  [state expr]
  (loop [state state
         args (rest expr)]
    (if (empty? args)
         [state true]
         (let [[state pred] (eval state (first args))]
           (if (not pred)
               [state false]
               (recur state (rest args)))))))

(defn eval-or
  "Evaluates (or exprs&). Halts evaluation after first `true`."
  [state expr]
  (loop [state state
         args (rest expr)]
    (if (empty? args)
         [state false]
         (let [[state pred] (eval state (first args))]
           (if pred
               [state true]
               (recur state (rest args)))))))

(defn eval-if
  "Evaluate if statement (if pred cons alt)"
  [state expr]
  (let [operands (rest expr)
        [state pred] (sub-eval state (first operands) 1)
        cons-expr (first (next operands))
        alt-expr (first (nnext operands))]
    (if pred
      (sub-eval state cons-expr 2)
      (sub-eval state alt-expr 3))))

(defn eval-cond
  "Evaluate conditional statement (cond (pred cons) (pred cons) ... (else cons))"
  [state expr]
  (eval state (parse-cond (rest expr))))

(defn eval-loop
  "Evaluate conditional statement (loop (& (symb val)) & body)"
  [state expr]
  (eval state (parse-loop (rest expr))))

(defn eval-let
  "Evaluate let statement (let ((symb expr) ...) expr ...)"
  [state expr]
  (let [bindings (first (rest expr))
        body (rest (rest expr))
        let-expr (parse-let bindings body)
        [state value] (eval (enter-frame state) let-expr)]
    [(exit-frame state) value]))

(defn eval-define
  "Evaluate variable definition (define symbol expr)"
  [state expr]
  (let [symbol (first (rest expr))]
    ; throw an error if symbol is not a symbol
    (if (not (symbol? symbol))
      (throw+ {:source :anglican
               :type :invalid-symbol
               :symbol symbol
               :message (str "Invalid variable name: " symbol)}))
    ; in order to ensure recursive function definitions work, we first create
    ; a dummy binding (so the symbol exists in the parent environment of
    ; the function body) and then reset its binding to the evaluated value
    (let [[state value] (sub-eval (def-var state symbol nil) (second (rest expr)) 2)]
      (reset-var! state symbol value)
      [state symbol])))

(defn eval-begin
  "Evaluate multiple expressions statement (begin & exprs)"
  [state expr]
  (let [[state values] (seq-eval state (rest expr) 1)]
    [state (last values)]))

(defn eval-lambda
  "Evaluate compound procedure (lambda args body)"
  [state expr]
  (let [args (fnext expr)
        body (nnext expr)]
    [state (init-compound-proc state args body)]))

(defn eval-system
  "Evaluate system statement (system key ...)"
  [state expr]
  (let [keys (map keyword (rest expr))
        value (get-in state (conj keys :system))]
    [state value]))

(defn sub-eval
  "Evaluates a sub-expression expr = (nth parent-expr pos)"
  [state expr pos]
  (let [[state res] (eval (enter-expr state pos) expr)]
    [(exit-expr state) res]))

(defn seq-eval
  "Evaluates expressions in order, retaining side-effects in state"
  ([state exprs]
    (seq-eval state exprs 0))
  ([state exprs pos]
    (loop [state state
           values  []
           exprs exprs
           pos pos]
     (if (empty? exprs)
         [state values]
         (let [[state value] (sub-eval state (first exprs) pos)]
           (recur state (conj values value) (rest exprs) (inc pos)))))))

(defn apply-invalid
  [state proc args]
  (throw+ {:source :anglican
           :type :not-callable
           :proc proc
           :message (str "Unrecognized procedure type: '" proc)}))

(defn apply-primitive
  [state proc args]
  (let [state (inc-count state :calls :apply-primitive)
        value (clj/apply (:proc proc) args)]
    [state value]))

(defn apply-compound
  [state proc args]
  ; check argument arity
  (if (and (not (symbol? (:args proc)))
           (not= (count args)
                 (count (:args proc))))
    (throw+ {:source :anglican
             :type :wrong-arity
             :proc-args (:args proc)
             :apply-args args
             :body (:body proc)
             :message (format "Expected (%d) arguments, got (%d), in procedure %s"
                              (count (:args proc))
                              (count args)
                              `(~'lambda ~(:args proc) ~@(:body proc)))}))
  (loop [state (enter-expr state 0)
         args args]
    (let [bindings (if (seq? (:args proc))
                     (zipmap (:args proc) args)
                     {(:args proc) args})
          state (-> state
                    (inc-count :calls :apply-compound)
                    (enter-frame bindings (:env proc)))
          [state values] (seq-eval state (:body proc) 1)
          return-value (last values)]
      ; check whether tail call was a recursion
      ; this is a bit of a hack right now
      (if (instance? RecursiveApplication return-value)
        (recur (-> state
                   (update-in [:env-stack] pop)
                   (update-in [:eval-addr] (fn [a] (conj (pop a) (inc (peek a))))))
               (:args return-value))
        [(exit-expr (exit-frame state)) return-value]))))

(defn apply-erp
  [state proc args]
  (let [[state s] (sample state proc args)]
    [(inc-count state :calls :apply-erp) (:value s)]))

(defn apply-core
  [state proc args]
  (clj/apply (:proc proc) state args))

(defn apply-xrp
  [state proc args]
  (let [[state s] (sample state proc args)]
    [(inc-count state :calls :apply-xrp) (:value s)]))

(defn apply-memoized
  [state proc args]
  (let [state (inc-count state :calls :apply-memoized)
        cache (get-in state [:proc-state (:addr proc)] {})]
    (if (contains? cache args)
        [(inc-count state :lookups :memoized) (cache args)]
        (let [[state value] (apply state (:proc proc) args)
              state (assoc-in state [:proc-state (:addr proc) args] value)]
          [state value]))))

(defn sample-invalid
  [state proc args]
  (throw+ {:source :anglican
           :type :cannot-sample
           :proc proc
           :message (str "Cannot sample from procedure type: '"
                         (class proc))}))
(defn sample-erp
  [state proc args]
  (let [value ((clj/apply (:rng proc) args))
        log-prob (clj/apply (:log-pdf proc) value args)
        s (Sample. value log-prob proc args)]
    [(inc-count state :calls :apply-erp) s]))

(defn sample-xrp
  [state proc args]
  ; assert that no arguments are supplied
  (if (not (empty? args))
    (throw+ {:source :anglican
             :type :wrong-arity
             :apply-args args
             :name (:name proc)
             :message (format "Expected (0) arguments, got (%d), in procedure %s"
                              (count args)
                              (:name proc))})
    (let [stats (get-in state [:proc-state (:addr proc)])
          value (clj/apply (:rng proc) stats (:args proc))
          log-prob (clj/apply (:log-pdf proc) stats  value (:args proc))
          stats ((:assoc proc) stats value)
          state (inc-count state :calls :sample-xrp)
          state (assoc-in state [:proc-state (:addr proc)] stats)]
      [state (Sample. value log-prob proc args)])))

(defn score-invalid
  [state proc value args]
  (throw+ {:source :anglican
           :type :cannot-score
           :proc proc
           :message (str "Cannot calculate log probability for procedure type: '"
                         (class proc))}))
(defn score-erp
  "Evaluates log probability of an ERP application value."
  [state proc args value]
  (clj/apply (:log-pdf proc) value args))

(defn score-xrp
  "Evaluates log probability of an XRP application value."
  [state proc args value]
  (let [stats (get-in state [:proc-state (:addr proc)])]
    (clj/apply (:log-pdf proc) stats value (:args proc))))


(defn init-compound-proc
  [state args body]
  (let [env (local-env state)]
    (CompoundProcedure. 'lambda args body env)))

(defn init-mem-proc
  [state proc]
  (let [addr (:eval-addr state)
        name (:name proc)]
    (MemoizedProcedure. name proc addr)))

(defn init-xrp
  [state proc args]
  ; initialize xrp by assigning address and args
  ; modify name to (name args&)
  (-> proc
      (assoc :name `(~(:name proc) ~@args))
      (assoc :addr (:eval-addr state))
      (assoc :args args)))

; addresses will eventually be implemented as a rolling hash
; for now we just use a vector [pos ...]
(defprotocol Address
  (sub-addr [addr pos])
  (parent-addr [addr]))

(extend-type nil
  Address
  (sub-addr [addr pos] [pos])
  (parent-addr [addr] nil))

(extend-type clojure.lang.IPersistentVector
  Address
  (sub-addr [addr pos] (conj addr pos))
  (parent-addr [addr] (pop addr)))

(defn self-evaluating? [expr]
  (or (nil? expr)
      (instance? Boolean expr)
      (number? expr)
      (string? expr)
      (procedure? expr)
      (and (list? expr)
           (empty? expr))))

(defn procedure? [expr]
  (contains? apply-methods (class expr)))

(defn parse-loop
  "Parses an expression of the form

    (loop ((symb val)
           ...
           (symb val))
      expr
      ...
      expr
      (recur arg ... arg))"
  [exprs]
  (let [bindings (first exprs)
        body (rest exprs)
        arg-names (map first bindings)
        arg-values (map second bindings)]
    `((~'lambda ~arg-names ~@body) ~@arg-values)))

(defn parse-cond
  "Parses an expression of the form

     (cond (pred cons)
           (pred cons)
           ...
           (else alt))

  into a set of nested if statements

    (if pred
        cons
        (if pred
            cons
            ...
                (if pred
                    cons
                    alt))))"
  [exprs]
  (if (empty? exprs)
      nil
      (let [clause (first exprs)
            pred (first clause)
            cons (second clause)]
           (if (= pred  'else)
               cons
               (list 'if pred cons (parse-cond (rest exprs)))))))


(defn parse-bindings [bindings]
  (if (empty? bindings)
    nil
    (lazy-seq (cons `(~'define ~(ffirst bindings) ~(second (first bindings)))
                    (parse-bindings (rest bindings))))))


(defn parse-let
  "Parses an expression of the form

     (let ((symbol expr)
           (symbol expr))
       expr
       ...
       expr)

  to a begin with defines

    (begin
      (define symb expr)
      ...
      (define symb expr)
      expr
      ...
      exp)
  "
  [bindings body]
  `(~'begin ~@(parse-bindings bindings) ~@body))


(defn local-env [state]
  (-> state
      :env-stack
      peek))

(defn get-var [state symbol]
  (loop [env (local-env state)]
    (if (contains? (:bindings env) symbol)
        @(get-in env [:bindings symbol])
        (if (some? (:parent env))
            (recur (:parent env))
            (throw+ {:source :anglican
                     :type :unbound-symbol
                     :symbol symbol
                     :message (str "Symbol not bound in environment: '" symbol)})))))

(defn def-var [state symbol value]
  (let [env (local-env state)]
    ; throw error if symbol is a reserved language keyword
    (if (contains? directives symbol)
        (throw+ {:source :anglican
                 :type :reserved-binding
                 :symbol symbol
                 :message (str "Cannot set binding of reserved keyword: '" symbol)}))
    ; throw error if symbol is already bound in local environment
    (if (contains? (:bindings env) symbol)
        (throw+ {:source :anglican
                 :type :existing-binding
                 :symbol symbol
                 :message (str "Cannot re-define symbol (already bound in local environment): '"
                               symbol)}))
    ; return state with updated environment
    (let [env-stack (:env-stack state)]
      (assoc state :env-stack (conj (pop env-stack)
                                    (assoc-in env [:bindings symbol] (atom value)))))))


(defn reset-var! [state symbol value]
  (loop [env (local-env state)]
    (if (contains? (:bindings env) symbol)
        (reset! (get (:bindings env) symbol) value)
        (if (some? (:parent env))
            (recur (:parent env))
            (throw+ {:source :anglican
                     :type :unbound-symbol
                     :symbol symbol
                     :message (str "Cannot reset symbol that is not bound in environment: '" symbol)})))))
(defn enter-frame
  ([state bindings parent-env]
   (let [env (EnvironmentFrame. (zipmap (keys bindings)
                                        (map atom (vals bindings)))
                                parent-env)]
     (update-in state [:env-stack] conj env)))
  ([state bindings]
   (enter-frame state bindings (local-env state)))
  ([state]
   (enter-frame state {})))

(defn exit-frame [state]
  (update-in state [:env-stack] pop))

(defn enter-expr [state pos]
  (update-in state [:eval-addr] #(sub-addr % pos)))

(defn exit-expr [state]
  (update-in state [:eval-addr] parent-addr))

(def eval-methods
  {:application eval-application
   :recur eval-recur
   :invalid eval-invalid
   :self-evaluating eval-self-evaluating
   :symbol eval-symbol
   :quote eval-quote
   :and eval-and
   :or eval-or
   :if eval-if
   :cond eval-cond
   :loop eval-loop
   :let eval-let
   :define eval-define
   :begin eval-begin
   :lambda eval-lambda
   :system eval-system
   })

(def apply-methods
  {PrimitiveProcedure apply-primitive
   CompoundProcedure apply-compound
   ElementaryRandomProcedure apply-erp
   CoreProcedure apply-core
   ExchangeableRandomProcedure apply-xrp
   MemoizedProcedure apply-memoized})

(def score-methods
  {ElementaryRandomProcedure score-erp
   ExchangeableRandomProcedure score-xrp})

(def sample-methods
  {ElementaryRandomProcedure sample-erp
   ExchangeableRandomProcedure sample-xrp})

(def intepreter-methods
  {:eval eval*
   :apply apply*
   :sample sample*
   :observe observe*
   :score score*})

(extend ExecutionState
  Interpreter
  intepreter-methods)

(def constants
  {'pi math/pi})


(def primitives
  (into {}
        (map
          (fn [pair]
            (let [[name proc] pair]
              [name (PrimitiveProcedure. name proc)]))
          {; tests
           'nil? clj/nil?
           'some? clj/some?
           'empty? clj/empty?
           'list? clj/list?
           'seq? clj/seq?
           'symbol? clj/symbol?
           'number? clj/number?
           'ratio? clj/ratio?
           'integer? clj/integer?
           'float? clj/float?
           'string? clj/string?
           'boolean? (partial instance? Boolean)
           'even? clj/even?
           'odd? clj/odd?
           'proc? (fn [p] (contains? apply-methods (class p)))
           ; relational
           'not= clj/not=
           '= clj/=
           '> clj/>
           '>= clj/>=
           '< clj/<
           '<= clj/<=
           ; scalar arithmetic
           'inc clj/inc
           'dec clj/dec
           'mod clj/mod
           ; custom math primitive
           'isfinite? math/isfinite?
           'isnan? math/isnan?
           ; matrix operations
           'array mat/array
           'slice mat/slice
           'emap mat/emap
           'scalar? mat/scalar?
           'vec? mat/vec?
           'matrix? mat/matrix?
           'array? mat/array?
           'eye (fn [size] (mat/identity-matrix size))
           'zeros (fn [& dims] (mat/zero-array dims))
           'eye? 'mat/identity-matrix?
           '== mop/==
           '+ mop/+
           '- mop/-
           'mmul mat/mmul
           '* mop/*
           '/ mop//
           ; TODO: include mop/** ?
           'abs mat/abs
           'floor mat/floor
           'ceil mat/ceil
           'round mat/round
           ;'rint mat/rint
           'signum mat/signum
           'sin mat/sin
           'cos mat/cos
           'tan mat/tan
           'asin mat/asin
           'acos mat/acos
           'atan mat/atan
           'sinh mat/sinh
           'cosh mat/cosh
           'tanh mat/tanh
           'log mat/log
           'log10 mat/log10
           'exp mat/exp
           'pow mat/pow
           'cbrt mat/cbrt
           'sqrt mat/sqrt
           'gammaln math/gammaln
           ; math primitives on sequences
           ; TODO: vectorize
           'sum math/sum
           'cumsum math/cumsum
           'mean math/mean
           'normalize math/normalize
           'range range
           ; casting - documented
           'boolean clj/boolean
           'double clj/double
           'long clj/long
           'read-string clj/read-string
           'str clj/str
           ; casting - undocumented
           'bigdec clj/bigdec
           'bigint clj/bigint
           'biginteger clj/biginteger
           'byte clj/byte
           'float clj/float
           'int clj/int
           'num clj/num
           'rationalize clj/rationalize
           'short clj/short
           ; data structures – documented
           'list list
           'first first
           'second second
           'nth nth
           'rest rest
           'sort sort
           'count count
           'unique distinct
           ; data structures - undocumented
           'into clj/into
           'concat clj/concat
           'hash-map clj/hash-map
           'array-map clj/array-map
           'vector clj/vector
           'conj clj/conj
           'pop clj/pop
           'peek clj/peek
           'vec clj/vec
           'seq clj/seq
           'set clj/set
           'zipmap clj/zipmap
           'get clj/get
           'get-in clj/get-in
           'assoc clj/assoc
           'assoc-in clj/assoc-in
           ; misc
           'prn prn
           'time time
           'nanotime nanotime
           'gensym gensym
           })))

(def erps
  (into {}
        (map (fn [pair]
               (let [[name [rng log-pdf]] pair]
                 [name (ElementaryRandomProcedure. name rng log-pdf)]))
             {'beta [erp/beta-rng erp/beta-lnpdf]
              'binomial [erp/binomial-rng erp/binomial-lnpdf]
              'categorical [erp/categorical-rng erp/categorical-lnpdf]
              'dirac [erp/dirac-rng erp/dirac-lnpdf]
              'dirichlet [erp/dirichlet-rng erp/dirichlet-lnpdf]
              'discrete [erp/discrete-rng erp/discrete-lnpdf]
              'discrete-cdf [erp/discrete-cdf-rng erp/discrete-cdf-lnpdf]
              'exponential [erp/exponential-rng erp/exponential-lnpdf]
              'flip [erp/flip-rng erp/flip-lnpdf]
              'gamma [erp/gamma-rng erp/gamma-lnpdf]
              'normal [erp/normal-rng erp/normal-lnpdf]
              'mvn [erp/mvn-rng erp/mvn-lnpdf]
              'diag-mvn [erp/diag-mvn-rng erp/diag-mvn-lnpdf]
              'wishart [erp/wishart-rng erp/wishart-lnpdf]
              'poisson [erp/poisson-rng erp/poisson-lnpdf]
              'uniform-continuous [erp/uniform-continuous-rng erp/uniform-continuous-lnpdf]
              'uniform-discrete [erp/uniform-discrete-rng erp/uniform-discrete-lnpdf]})))

(def xrp-templates
  (into {}
        (map (fn [entry]
               (let [[name [rng log-pdf assoc dissoc]] entry]
                 [name (ExchangeableRandomProcedure. name rng log-pdf assoc dissoc nil nil)]))
             {'crp [xrp/crp-rng xrp/crp-lnpdf xrp/crp-assoc xrp/crp-dissoc]
              'flip-beta [xrp/flip-beta-rng
                          xrp/flip-beta-lnpdf
                          xrp/flip-beta-assoc
                          xrp/flip-beta-dissoc]
              'discrete-dirichlet [xrp/discrete-dirichlet-rng
                                   xrp/discrete-dirichlet-lnpdf
                                   xrp/discrete-dirichlet-assoc
                                   xrp/discrete-dirichlet-dissoc]
              'discrete-sdirichlet [xrp/discrete-sdirichlet-rng
                                    xrp/discrete-sdirichlet-lnpdf
                                    xrp/discrete-sdirichlet-assoc
                                    xrp/discrete-sdirichlet-dissoc]
              'normal-with-known-std
                [xrp/normal-with-known-std-rng xrp/normal-with-known-std-lnpdf
                 xrp/normal-with-known-std-assoc xrp/normal-with-known-std-dissoc]
              'normal-unknown-mean [xrp/normal-unknown-mean-rng
                                    xrp/normal-unknown-mean-lnpdf
                                    xrp/normal-unknown-mean-assoc
                                    xrp/normal-unknown-mean-dissoc]})))

(def xrp-generators
  (into {}
        (map (fn [entry]
               (let [[name proc] entry
                     core-proc (fn [state & args]
                                [state (init-xrp state proc args)])]
                 [name (CoreProcedure. name core-proc)]))
             xrp-templates)))

(def core-procs
  (into {}
        (map (fn [entry]
               (let [[name proc] entry]
                 [name (CoreProcedure. name proc)]))
             {'eval (fn [state & args]
                      (eval state
                            (first args)))
              'apply (fn [state & args]
                        (apply state
                               (first args)
                               (second args)))
              'mem (fn [state & args]
                     [state (init-mem-proc state (first args))])})))

; global environment containing language primitives
(def global-env
  (let [globals (merge constants
                       primitives
                       erps
                       xrp-generators
                       core-procs)]
    (EnvironmentFrame.
      (zipmap (keys globals) (map atom (vals globals)))
      nil)))

; empty local environment with global as parent
(def empty-env
  (EnvironmentFrame. {} nil))

; empty execution state
(def empty-state
  (ExecutionState.
   {}
   [global-env]
   [:global]
   {}))

(defn inc-count [state & keys]
  (update-in state (conj keys :system) (fnil inc 0)))

(defn reset-count [state & keys]
  (assoc-in state (conj keys :system) 0))

(defn reset-time [state]
  (assoc-in state [:system :start-time] (. System (nanoTime))))
