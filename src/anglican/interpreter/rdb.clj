(ns anglican.interpreter.rdb
  (:refer-clojure :exclude [eval apply])
  (:require [anglican.interpreter.base
             :as base
             :refer [Interpreter
                     eval apply sample score observe
                     expr-type eval-invalid apply-invalid inc-count]]
            [slingshot.slingshot :refer [throw+]] :reload)
  (:import [anglican.interpreter.base
              Sample ElementaryRandomProcedure ExchangeableRandomProcedure]))

(declare eval-methods apply-methods
         apply-random-procedure
         get-cached assoc-cached assoc-sampled assoc-observed
         can-rescore? equal-dim?)

; An execution state contains
; {:type namespace-qualified-keyword
;  :env-stack   [EnvironmentFrame ...]
;  :proc-state  {addr {} ...}
;  :rand-state  {addr Sample ...}
;  :call-stack  [addr ...]
;  :eval-addr  addr
;  :system  {}}
(defrecord ExecutionState
  [env-stack proc-state rand-state call-stack eval-addr system])

(defn apply-random-procedure
  [state proc args]
  "Application dispatch for random procedures. Looks up sample in cache,
  rescores under new args if possible, and draws new sample if either of
  the preceding tests fail."
  (let [cached (get-cached state)]
    (if (or (nil? cached) (not (can-rescore? cached proc args)))
      ; draw a new sample
      (let [[state s] (sample state proc args)]
        ; return new sample
        [(assoc-sampled state s) (:value s)])
      ; if in cache, check whether args are identical
      (if (= (:args cached) args)
        ; return cached sample
        [(assoc-cached state cached) (:value cached)]
        ; if args have changed we need to rescore
        (let [log-prob (score state proc args (:value cached))
              rescored (assoc cached :log-prob log-prob :args args)]
          ; return rescored sample
          [(assoc-cached state rescored) (:value rescored)])))))

(defn get-cached [state]
  "Get cached random choice if exists at the current address."
  (let [addr (:eval-addr state)]
    (get-in state [:rand-state :cached addr])))

(defn add-logprobability
  [state key addition]
  "Accumulates spent/restored/observed logprobability to the current execution."
  (let
    [current-logprobability (get (:logprobabilities (:system state)) key)
     current-logprobability (if (nil? current-logprobability) 0 current-logprobability)
     sum (+ current-logprobability addition)]
    (assoc-in state [:system :logprobabilities key] sum)))

(defn logprob-from-sample
  [state s]
  "Get the logprobability of the random choice."
  (score state (:proc s) (:args s) (:value s)))

(defn assoc-cached [state s]
  "Put the random choice, which has been restored from the cached set, to the set of reused
   random choices. The logprobability will be either accumulated to the <<restored probability>>
   counter or artificially to the <<spent probability>> counter. The latter happens
   if we simulating the sampling of random choices in order to calculate Q(X_new -> X_old)."
  (let [addr (:eval-addr state)
        probability-key
          (if (and (contains? (:system state) :consider-as-new) (contains? (get-in state [:system :consider-as-new]) addr))
            :spent-probability
            :restored-probability)
        state (add-logprobability state probability-key (logprob-from-sample state s))
        state
          (if (= (type (:proc s)) ExchangeableRandomProcedure)
            (let
              [stats (get-in state [:proc-state (:addr (:proc s))])
               stats ((:assoc (:proc s)) stats (:value s))]
              (assoc-in state [:proc-state (:addr (:proc s))] stats))
            state)]
    (assoc-in state [:rand-state :reused addr] s)))

(defn assoc-sampled [state s]
  "Put the random choice to the set of sampled random choices, i.e. when the probability
   actually was spent."
  (let [addr (:eval-addr state)
        state (add-logprobability state :spent-probability (logprob-from-sample state s))]
    (assoc-in state [:rand-state :sampled addr] s)))

(defn assoc-observed [state s]
  "Put the random choice to the set of observed random choices."
  (let [addr (:eval-addr state)
        state (add-logprobability state :observed-probability (logprob-from-sample state s))]
    (assoc-in state [:rand-state :observed addr] s)))

(defn can-rescore?
  [s proc args]
  "Checks whether a sample can be rescored given a new procedure and args"
  (and (= (:proc s) proc)
       (equal-dim? (:args s) args)))

(defn equal-dim?
  [a b]
  "Checks if a and b have same dimension. If a and b are collections,
  each element of a and b is checked recursively."
  (if (not (or (coll? a) (coll?  b)))
      true
      (if (and (coll? a) (coll? b) (= (count a) (count b)))
          (every? true? (map equal-dim? a b))
          false)))

(def apply-methods
     (merge base/apply-methods
            {ElementaryRandomProcedure apply-random-procedure
             ExchangeableRandomProcedure apply-random-procedure}))

(defn apply*
  [state proc args]
  (let [state (inc-count state :calls :apply)
        apply-fn (get apply-methods (class proc) apply-invalid)]
    (apply-fn state proc args)))

(defn observe*
  [state expr value]
  (let [[state s] (base/observe* state expr value)]
    [(assoc-observed state s) s]))

(def intepreter-methods
  (assoc base/intepreter-methods
         :apply apply*
         :observe observe*))

(extend ExecutionState
  Interpreter
  intepreter-methods)

; empty execution state
(def empty-state
  (ExecutionState.
   [base/global-env]
   {}
   {}
   [[:global]]
   [:global]
   {}))
