(ns anglican.math.erp
  (:refer-clojure :exclude [rand rand-int])
  (:require [slingshot.slingshot :refer [throw+]]
            [anglican.math
             :refer [log exp sqrt rand randn rand-int mersenne-twister gammaln cumsum pi sample-cdf]]
            [clojure.core.matrix :as mat]
            [clojure.core.matrix.linear :as linalg]
            [clojure.core.matrix.operators :as mop])
  (:import (cern.jet.random Gamma Binomial Exponential Poisson Normal)
           (cern.jet.stat Probability)))


(mat/set-current-implementation :vectorz)

(declare dirichlet-rng dirichlet-lnpdf discrete-rng gamma-rng gamma-lnpdf)

(defn beta-rng
  "Returns a function that samples from a Beta distribution with
  with pseudocounts a and b."
  [a b]
  (let [rng (dirichlet-rng [b a])]
    (fn [] (second (rng)))))

(defn beta-lnpdf
  "Log-probability density of a sample mu drawn from a Beta distribution
  with pseudocounts a and b."
  [mu a b]
  (dirichlet-lnpdf [mu (- 1 mu)] [b a]))


(defn binomial-rng
  "Returns a function that samples from a Binomial distribution with success
  probability p and number of trials n."
  [p n]
  (let
    [binobj (ref (new Binomial n p @mersenne-twister))]
    (fn [] (. @binobj nextInt))))

(defn binomial-lnpdf
  "Log-probability density of a sample t drawn from a Binomial distribution
  with success probability p and number of trials n."
  [t p n]
  (+ (+ (- (gammaln (+ n 1))
           (+ (gammaln (+ t 1))
              (gammaln (+ (- n t) 1))))
        (* t (log p)))
     (* (- n t) (log (- 1 p)))))

(defn bernoulli-rng [p]
  (binomial-rng p 1))

(defn bernoulli-lnpdf [t p]
  (binomial-lnpdf t p 1))

(defn categorical-rng
  [rho-map]
  (fn []
    (let [values (map first rho-map)
          rho (map second rho-map)
          n ((discrete-rng rho))]
      (nth values n))))

(defn categorical-lnpdf
  [k rho-map]
  (let [pdf (into {} (map vec rho-map))]
   (log (get pdf k 0.0))))

(defn dirac-rng
  "Delta kernel. Samples value v with probability 1."
   [v0]
   (fn [] v0))

(defn dirac-lnpdf
  "Delta kernel. Samples value v with probability 1."
   [v v0]
   (if (= v v0)
     0.0
     Double/NEGATIVE_INFINITY))

(defn dirichlet-rng
  "Returns a function that samples from a Dirichlet distribution with
  with pseudocounts alpha."
  [alpha]
  (fn []
    (let [g (map #((gamma-rng % 1)) alpha)
          t (reduce + g)]
      (apply list (map #(/ % t) g)))))

(defn dirichlet-lnpdf
  "Log-probability density of a sample mu drawn from a Dirichlet distribution
  with pseudocounts alpha."
  [mu alpha]
  (let [p (reduce + (map #(* (- %1 1) (log %2)) alpha mu))
        Z (- (gammaln (reduce + alpha)) (reduce + (map gammaln alpha)))]
    (+ p Z)))

(defn discrete-cdf-rng
  "Returns a function that samples from a discrete distribution with cumulative
  densities x = [x1 x2 ... 1]"
  [x]
  (if (> (Math/abs (- (last x) 1.0)) 1e-12)
    (throw+ {:source :anglican
             :type :value-error
             :proc-name 'discrete-cdf
             :message (str "discrete-cdf takes a sequence of cumulative probabilities that must increase monotonically to 1, last value supplied is " (last x))}))
  (fn []
    (sample-cdf x)))

(defn discrete-cdf-lnpdf
  "Log probability density of a sample n drawn from a discrete distribution with
  cumulative densities x"
  [n x]
  (if (= 0 n)
      (log (nth x n))
      (log (- (nth x n)
              (nth x (dec n))))))

(defn discrete-rng
  "Returns a function that samples from a discrete distribution with probability
  densities rho = [rho1 rho2 ... rhoK]"
  [rho]
  (let [cdf (cumsum rho)]
    (if (> (Math/abs (- (last cdf) 1.0)) 1e-12)
      (throw+ {:source :anglican
               :type :value-error
               :proc-name 'discrete-rng
               :message (str "discrete takes a sequence of probabilities that must sum to 1, supplied values sum to " (last cdf))}))
    (discrete-cdf-rng cdf)))

(defn discrete-lnpdf
  "Log probability density of a sample n drawn from a discrete distribution with
  densities rho"
  [n rho]
    (log (nth rho n)))

(defn exponential-rng
  "Returns a function that samples from an Exponential distribution with
  with rate parameter lambda."
  [lambda]
  (let [expobj (ref (new Exponential (/ 1.0 lambda) @mersenne-twister))]
    (fn [] (. @expobj nextDouble))))

(defn exponential-lnpdf
  "Log-probability density of a sample t drawn from an Exponential distribution
  with rate parameter lambda."
  [t lambda]
  (- (log (/ 1.0 lambda)) (* (/ 1.0 lambda) t)))

(defn flip-rng
  "Returns a function that samples from a discrete distribution with weights
  [1-w w]"
  [w]
   (fn []
     (let [r (rand)]
       (<= r w))))

(defn flip-lnpdf
  "Log probability density for a boolean b drawn from a discrete distribution
  with weights [1-w w]"
  [b w]
  (if (= b true) (log w)
    (if (= b false) (log (- 1 w))
        (log 0.0))))

(defn gamma-rng
  "Returns a function that samples from a Gamma distribution with shape a
  and rate b (Bishop/Murphy notation)."
  [a b]
  (let [gamobj (ref (new Gamma a b @mersenne-twister))]
      (fn [] (. @gamobj nextDouble))))

(defn gamma-lnpdf
  "Log probability density of a sample t drawn from a Gamma distribution
  with shape a and rate b (Bishop/Murhpy notation)"
  [t a b]
  (+ (+ (+ (- 0 (. cern.jet.stat.Gamma logGamma a))
           (* a (log b)))
        (* (- a 1)
           (log t)))
     (- 0 (* b t))))

(defn gamma-cdf
  [t a b]
  (/ (. cern.jet.stat.Gamma incompleteGamma a (* b t))
     (. cern.jet.stat.Gamma gamma a)))

(defn invgamma-rng
  "Returns a function that samples from an inverse-Gamma distribution with
  shape a and rate b (Bishop/Murphy notation)."
  [alpha beta]
  (let [gamobj (ref (new Gamma alpha (/ 1.0 beta) @mersenne-twister))]
    (fn [] (/ 1.0 (. @gamobj nextDouble)))))

(defn invgamma-lnpdf
  "Log probability density of a sample t drawn from an inverse-Gamma distribution
  with shape a and rate b (Bishop/Murhpy notation)"
  [t a b]
  (+ (+ (+ (- 0.0 (. cern.jet.stat.Gamma logGamma a))
           (* a (log b)))
        (* (- (+ a 1)) (log t)))
     (- 0.0 (/ b t))))

(defn normal-rng
  "Returns a function that samples from a univariate normal distribution with
  known mean and standard deviation"
  [mean std]
  (fn []
    (randn mean std)))

(defn normal-lnpdf
  "Log probability density of a sample x drawn from a univariate normal with
  known mean and standard deviation"
  [x mean std]
  (let [dx (- x mean)
        var (* std std)]
    (* -0.5
       (+ (log (* 2 (* pi var)))
          (/ (* dx dx) var)))))

(defn normal-cdf [x mean std]
  "Cumulative probability density of a sample x drawn from a univariate normal
  with mean mean and standard deviation std"
  (+ 0.5
     (* 0.5 (. cern.jet.stat.Probability errorFunction
               (/ (- x mean)
                  (* (sqrt 2) std))))))

(defn poisson-rng
  "Returns a function that samples from a Poisson distribution with
  with rate parameter lambda."
   [lambda]
   (let [lamobj (ref (new Poisson lambda @mersenne-twister))]
     (fn [] (. @lamobj nextInt))))

(defn poisson-lnpdf
  "Log-probability density of a sample k drawn from an Poisson distribution
  with rate parameter lambda."
  [k lambda]
  (- (- (* k (log lambda)) lambda) (gammaln (+ k 1))))

(defn uniform-continuous-rng
  "Returns a function that samples from a uniform continous distribution with
  with support [min, max]"
  ([min max]
    (fn []
      (rand min max))))

(defn uniform-continuous-lnpdf
  "Log probability density for a continuous distribution"
  ([value min max]
    (if (and (>= value min) (<= value max))
        (* -1. (log (- max min)))
        (log 0.))))

(defn uniform-discrete-rng
  "Returns a function that samples from a uniform discrete distribution with
  with support [min ... max-1]"
  ([min max]
    (fn []
      (rand-int min max))))

(defn uniform-discrete-lnpdf
  "Log probability density of a value n drawn from a uniform discrete
  distribution with support [min ... max-1]"
  ([n min max]
    (uniform-continuous-lnpdf n min max)))

(defn mvn-rng
  "Returns a function that samples from a multivariate normal distribution with
  known mean vector and covariance matrix"
  [mean cov]
  (fn []
    (mop/+
     mean
     (mat/mmul
      (:L (linalg/cholesky (mat/matrix cov)))
      (repeatedly (count mean) (fn [] (randn 0 1)))))))

(defn mvn-lnpdf
  "Log probability density of a sample x drawn from a multivariate normal with
  known mean vector and covariance matrix"
  [x mean cov]
  (let [dim (count mean)
        lower (:L (linalg/cholesky (mat/matrix cov)))
        log-det (log (reduce * (mat/diagonal lower)))
        ; TODO sadly this does not know about the structure of "lower"
        ; TODO also, sadly, `linalg/solve` is not yet implemented
        L-dx (mat/mmul (mat/inverse lower) (mop/- x mean))]
    (* -0.5
       (+
          (* dim (log (* 2 pi)))
          log-det
          (mat/dot L-dx L-dx)))))

(defn diag-mvn-rng
  "Returns a function that samples from a multivariate normal distribution with
  known mean vector and _diagonal_ covariance matrix.
  Input arguments are the list of means and the list of variances.
  For example,
  (diag-mvn (list 0.0 1.0 2.0) (list 1.0 1.0 1.0))
  samples a 3d vector of 3 independent normal random variates distributed
  by N(0.0, 1.0), N(1.0, 1.0), N(2.0, 1.0) correspondingly."
  [means variances]
  (fn []
    (doall (map (fn [mean variance] (randn mean (sqrt variance))) means variances))))

(defn diag-mvn-lnpdf
  "Log probability density for diag-mvn. See diag-mvn-rng description for details."
  [xs means variances]
  (let [current-lnpdf
    (apply +
      (map
        (fn [x mean variance] (normal-lnpdf x mean (sqrt variance)))
        xs
        means
        variances))]
    current-lnpdf))

(defn wishart-rng
  "Returns a function that samples from a wishart distribution with
  nu degrees of freedom and a d x d positive definite scale matrix V,
  where nu > d - 1.  This is the conjugate prior for a precision matrix."
  [nu V]
  (fn []
    ; This is a simple sampling algorithm, which handles integer degrees of freedom.
    (let [nu (int nu)
          d (first (mat/shape V))
          L (:L (linalg/cholesky (mat/matrix V)))
          X (mat/matrix (repeatedly nu (fn [] (mat/mmul L (repeatedly d (fn [] (randn 0 1)))))))]
      (mat/mmul (mat/transpose X) X))))

(defn wishart-lnpdf
  "Log probability density of a positive-definite matrix sample X drawn from
  a wishart distribution with nu degrees of freedom and a d x d positive definite
  scale matrix V, where nu > d - 1"
  [X nu V]
  (let [d (first (mat/shape V))
        ln-gamma-val (fn [i] (gammaln (* 0.5 (- (+ nu 1) i))))
        ; compute tr(V^{-1}X) without constructing entire V^{-1}X
        trace-V-inv-X (mat/esum (mop/* (mat/inverse V) X))]
       (+
        (* -0.5 trace-V-inv-X)
        (* 0.5 (- nu (+ d 1)) (log (mat/det X)))
        (* -0.5 nu (log (mat/det V)))
        (* -0.5 nu d (log 2))
        (* -0.25 d (- d 1) (log pi))
        (reduce + (map ln-gamma-val (range 1 (+ d 1)))))))
