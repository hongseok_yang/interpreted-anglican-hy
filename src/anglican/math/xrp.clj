(ns anglican.math.xrp
  (:require [anglican.math :as math :refer [log exp sqrt cdf sample-cdf sum]]
            [anglican.math.erp :as erp]
            [slingshot.slingshot :refer [throw+]]))

(defmacro dbg[x] `(let [x# ~x] (println "dbg:" '~x "=" x#) x#))

(defn crp-rng
  "Samples from Chinese restaurant process. counts is a vector,
  alpha is a scalar concentration parameter."
  [counts alpha]
  (let [counts (or counts {})
        ks (conj (keys counts)
                 (inc (apply max (conj (keys counts) 0))))
        cdf-ns (cdf (conj (vals counts) alpha))
        k (sample-cdf cdf-ns)]
    (nth ks k)))

(defn crp-lnpdf
  "Returns log probabiliy of a CRP sample"
  [counts index alpha]
  (let
    [counts (or counts {})
     denominator-log (log (reduce + alpha (vals counts)))]
    (if (contains? counts index)
      (- (log (get counts index)) denominator-log)
      (- (log alpha) denominator-log))))

(defn crp-assoc
  [counts index]
  (let [counts (or counts {})]
    (update-in counts [index] (fn [c] (inc (or c 0))))))

(defn crp-dissoc
  [counts index]
  (let [counts (or counts {})
        c (or (get counts index) 0)]
    (cond
      (> c 1) (update-in counts [index] dec)
      (= c 1) (dissoc counts index)
      (< c 1) (throw
                (new RuntimeException
                     (str "Cannot dissociate from count with value smaller than 1"))))))

(defn flip-beta-rng
  [counts a b]
  (let [ca (+ (get counts true 0) a)
        cb (+ (get counts false 0) b)
        q (/ ca (+ ca cb))]
    ((erp/flip-rng q))))

(defn flip-beta-lnpdf
  [counts f a b]
  (let [ca (+ (get counts true 0) a)
        cb (+ (get counts false 0) b)
        q (/ ca (+ ca cb))]
    (erp/flip-lnpdf f q)))

(defn flip-beta-assoc
  [counts f]
  (crp-assoc counts f))

(defn flip-beta-dissoc
  [counts f]
  (crp-dissoc counts f))

;(def mem-sum (memo/lru sum))

(defn discrete-dirichlet-rng
  [counts alpha]
  (let [norm (+ (sum (vals counts)) (sum alpha))
        w (map-indexed (fn [n a]
                         (/ (+ (get counts n 0.0) a)
                            norm))
                        alpha)]
    ((erp/discrete-rng w))))

(defn discrete-dirichlet-lnpdf
  [counts n alpha]
  (let [norm (+ (sum (vals counts)) (sum alpha))
        w (map-indexed (fn [n a]
                         (/ (+ (get counts n 0.0) a)
                            norm))
                        alpha)]
    (erp/discrete-lnpdf n w)))

(def discrete-dirichlet-assoc crp-assoc)

(def discrete-dirichlet-dissoc crp-dissoc)

(defn discrete-sdirichlet-rng
  [counts a k]
  (let [cs (into {} (map (fn [[n c]] [n (+ a c)]) counts))
        alpha (* a (- k (count counts)))
        n (crp-rng cs alpha)]
    (if (contains? cs n)
      ; return existing index
      n
      ; sample index from unused indices uniformly at random
      (let [unused (filterv (fn [n] (not (contains? cs n)))
                            (range k))]
        (rand-nth unused)))))

(defn discrete-sdirichlet-lnpdf
  [counts n a k]
  (if (or (< n 0)
          (>= n k))
    (Double/NEGATIVE_INFINITY)
    (let [log-norm (log (+ (sum (vals counts)) (* a k)))]
      (- (log (+ a (get counts n 0.0))) log-norm))))

(discrete-sdirichlet-lnpdf {1 10} 1 1.0 10)

(def discrete-sdirichlet-assoc crp-assoc)

(def discrete-sdirichlet-dissoc crp-dissoc)

;; this implementation is currently wrong
;; - we should only keep sufficient statistics of the state, i.e. count and sum
;; - current_var = updated_var + hyperparameter_var instead of
;;   current_var = updated_var
;;   (see http://en.wikipedia.org/wiki/Conjugate_prior#Continuous_distributions)
(defn normal-unknown-mean-helper
  [stats mu0 std0 std]
  (let
    [x-sum (get stats :x-sum 0)
     x-count (get stats :x-count 0)
     var0 (* std0 std0)
     var (* std std)
     current-mean (/ (+ (/ mu0 var0)
                        (/ x-sum var))
                     (+ (/ 1 var0)
                        (/ x-count var)))
     current-std (math/sqrt (+ var
                               (/ 1
                                  (+ (/ 1 var0)
                                     (/ x-count var)))))]
    (current-mean current-std)))

(defn normal-unknown-mean-rng
  [stats mu0 std0 std]
  (let
    [stats (or stats {:x-sum 0 :x-count 0})
     [current-mean current-std] (normal-unknown-mean-helper stats mu0 std0 std)]
    ((erp/normal-rng current-mean current-std))))

(defn normal-unknown-mean-lnpdf
  [stats value mu0 std0 std]
  (let
    [stats (or stats {:x-sum 0 :x-count 0})
     [current-mean current-std] (normal-unknown-mean-helper stats mu0 std0 std)]
    (erp/normal-lnpdf value current-mean current-std)))

(defn normal-unknown-mean-assoc
  [stats value]
  (let
    [x-sum (get stats :x-sum 0)
     x-count (get stats :x-count 0)]
    (do
      (update-in stats [:x-sum] (fn [x] (+ x value)))
      (update-in stats [:x-count] inc))))

(defn normal-unknown-mean-dissoc
  [stats value]
  (let
    [x-sum (get stats :x-sum 0)
     x-count (get stats :x-count 0)]
    (if (> x-count 0)
      (do
        (update-in stats [:x-sum] (fn [x] (- x value)))
        (update-in stats [:x-count] dec))
      (throw
       (new RuntimeException
            (str "Cannot dissociate from count with value smaller than 1"))))))
