(ns anglican.parser
  (:require [clojure.data.csv :as csv]
            [slingshot.slingshot :refer [try+ throw+]]
            [blancas.kern.core :as kern :refer :all :exclude [predict]]
            [blancas.kern.i18n :refer [i18n]]
            [blancas.kern.lexer :as lex]
            [clojure.pprint :as pprint]))

(def anglican-style
  (assoc lex/basic-def
    :comment-line       ";"
    :nested-comments     true
    :identifier-letter   (none-of* " \t\r\n'()[]")
    :identifier-start    (none-of* " \t\r\n'()[]$0123456789")
    :case-sensitive      true
    :trim-newline        true))

(def- rec (lex/make-parsers anglican-style))

(def trim       (:trim       rec))
(def lexeme     (:lexeme     rec))
(def sym        (:sym        rec))
(def token      (:token      rec))
(def new-line   (:new-line   rec))
(def word       (:word       rec))
(def string-lit (:string-lit rec))
(def dec-lit    (:dec-lit    rec))
(def bool-lit   (:bool-lit   rec))
(def parens     (:parens     rec))
(def brackets   (:brackets   rec))
(def comma-sep  (:comma-sep  rec))
(def identifier (:identifier rec))

(declare lisp parse-anglican)

(defrecord ColumnReference [column-id])
(defrecord AnglicanStatement [command code interpretation line])

(def nil-lit
     (>> (word "nil") (return nil)))

(def float-lit
  (>>= (<:> (lexeme
             (<+> (optional (one-of* "+-"))
                  (<|> (<:> (<*> (option "0" (many1 digit))
                                 (sym* \.)
                                 (many1 digit)))
                       (<:> (<*> (many1 digit)
                                 (sym* \.)
                                 (option "0" (many1 digit)))))
                  (optional (<*> (one-of* "eE") (optional (one-of* "+-")) (many1 digit)))
                  (<< (optional (sym* \M)) (not-followed-by letter)))))
       (fn [x] (>> (return (read-string x)) clear-empty))))

(def lit
  (<|> string-lit nil-lit bool-lit dec-lit float-lit))

(def symb
  (bind [name identifier]
    (return (symbol name))))

(def col-ref
  (bind
    [_ (sym* \$)
     i dec-lit]
    (return (ColumnReference. i))))

(def s-expr
  (bind [elements (parens (many (fwd lisp)))]
    (return (apply list elements))))

(def quoted
  (bind [_ (sym* \')
         body lisp]
        (return (list 'quote body))))

(def vect
  (bind [elements (brackets (many (fwd lisp)))]
    (return (cons 'vector elements))))

(def lisp
  (<|> quoted s-expr vect col-ref lit symb))

(def assume
  (<:>
   (brackets
    (bind [_ (<|> (word "assume") (word "ASSUME"))
           s lisp
           e lisp]
          (return (AnglicanStatement.
                   "assume"
                   (format "[assume %s %s]" s, e)
                   (list 'define s e)
                   nil))))))

(def observe
  (<:>
   (brackets
    (bind [_ (<|> (word "observe") (word "OBSERVE"))
           e lisp
           v lisp]
          (return (AnglicanStatement.
                   "observe"
                   (format "[observe %s %s]" e v)
                   (list 'observe e v)
                   nil))))))

(def predict
  (<:>
   (brackets
    (bind [_ (<|> (word "predict") (word "PREDICT"))
           e lisp]
          (return (AnglicanStatement.
                   "predict"
                   (format "[predict %s]" e)
                   e
                   nil))))))

(defn parse-csv-entry
  [entry]
  (cond
    (= entry "nil") nil
    (not (nil? (value (<|> dec-lit float-lit bool-lit) entry)))
      (value (<|> dec-lit float-lit bool-lit) entry)
    :else entry))

(defn lookup-col-refs
  [expr row]
  (cond
    (seq? expr)
      (map lookup-col-refs expr (repeat row))
    (instance? ColumnReference expr)
      (do
        (if (>= (dec (:column-id expr)) (count row))
          (throw+ {:source :anglican
                   :type :parse-error
                   :message (str "The requested column $" (:column-id expr) " does not exist for at least one of rows. Maybe you have extra empty lines?")})
          (parse-csv-entry (nth row (dec (:column-id expr))))))
    :else expr))

(def observe-csv
  (<:>
   (brackets
    (bind [_ (<|> (word "observe-csv") (word "OBSERVE-CSV"))
           u string-lit
           e lisp
           v lisp]
          (let
            [csv-data (doall (csv/read-csv (slurp u)))]
            (return (mapv (fn [row]
                            (let
                              [e (lookup-col-refs e row)
                               v (lookup-col-refs v row)]
                              (AnglicanStatement.
                                 "observe"
                                 (str "[observe " (pr-str e) " " (pr-str v) "]")
                                 (list 'observe e v)
                                 nil)))
                          csv-data)))))))

(def import-statement
  (<:>
    (brackets
      (bind
        [_ (<|> (word "import") (word "IMPORT"))
         url lisp
         ; optional-namespace (optional symb)
        ]
        (let
          [loading-url
            (if (string? url)
              url
              (if (and (seq? url) (= (first url) 'quote))
                (clojure.java.io/resource (str (second url) ".ang"))
                (throw+ {:source :anglican
                                 :type :parse-error
                                 :message (str "[import 'smth] or [import \"smth\"] only. Please, read documentation on directives for details.")})))
           _ (if (nil? loading-url)
               (throw+ {:source :anglican
                                :type :parse-error
                                :message (str "Requested library, " (second url) ", was not found in Anglican resources.")}))
           source-code (slurp loading-url)]
           (return (value parse-anglican source-code)))))))

(def anglican
  (<|> assume predict observe observe-csv import-statement))

(def parse-lisp
  (>> trim (many1 lisp)))

(def parse-anglican
     (>> trim (many1 anglican)))

(defn add-line-ids
  ([code prefix]
    (map-indexed (fn [n line]
                   (let [id (str prefix ":" n)]
                     (if (instance? AnglicanStatement line)
                       (assoc line :line id)
                       (add-line-ids line id))))
                 code))
  ([code]
   (add-line-ids code "")))

(defn anglican-str [text]
  (let [parser-state (parse parse-anglican text)]
    (if (some? (:error parser-state))
      (let [pos (get-in parser-state [:error :pos])]
        (throw+ {:source :anglican
                 :type :parse-error
                 :error (:error parser-state)
                 :message (str "Unable to parse at line " (:line pos) ", column " (:col pos) ".")})))
    (if (not (empty? (:input parser-state)))
      (throw+ {:source :anglican
               :type :parse-error
               :error (:error parser-state)
               :message (str "Some text could not be parsed: " (apply str (:input parser-state)))}))
    (into [] (flatten (add-line-ids (:value parser-state))))))

(defn anglican-file [filename]
  (let [source-text (slurp filename)]
    (anglican-str source-text)))

(defn anglican-reader [s]
  (let [source-text (char-seq (java.io.BufferedReader. s))]
    (anglican-str source-text)))

(defn lisp-file [filename]
  (let
    [source-code (slurp filename)]
    (add-line-ids (value parse-lisp source-code))))

(defn lisp-reader [s]
  (value parse-lisp (char-seq (java.io.BufferedReader. s))))
