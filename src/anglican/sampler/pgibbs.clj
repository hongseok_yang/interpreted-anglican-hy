(ns anglican.sampler.pgibbs
  (:refer-clojure :exclude [eval apply])
  (:require [clojure.core :as clj]
            [clojure.core.reducers :as r]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [anglican.math :as math :refer
             [isfinite? log exp mean log-sum-exp cdf-exp sample-cdf sample-log-weights]]
            [anglican.interpreter.base :refer [dbg reset-time get-var sub-eval enter-expr exit-expr observe]]
            [anglican.interpreter.smc :refer [empty-state log-weight log-prop]]
            [anglican.sampler :refer [*predict* *sample-count* *map*]]
            [anglican.sampler.smc :as smc
             :refer [ImportanceSampler run-line resample new-samples? need-resample? *resample-method*]]))

; controls whether predicts are evaluated for all particles, or just retained
(def ^:dynamic *retained-only* false)

(declare fork)

; A PGibbs particle set contains
;
; states [ExecutionState ...]
;   L-1 execution states for each sampled particle.
;
; ancestries [[ExecutionState ...] ...]
;   L state lineages of past generations for each particle.
;   (peek ancestries) holds n generations of states for
;   retained particle. (pop ancestries) holds L-1 ancestries
;   for new particles.
;
; retained [ExecutionState ...]
;   holds N-n continuation states for retained particle.
(defrecord ParticleSet [states ancestries retained])

(defnp run-assume
  [particles line]
  (let [particles (smc/run-assume particles line)]
    (if (nil? (:retained particles))
      ; if doing smc, just update ancestries
      (let [ancestries (into [] (*map* conj
                                       (:ancestries particles)
                                       (:states particles)))]
        (assoc particles :ancestries ancestries))
      ; if doing csmc, update ancestries and retained future
      (let [states (conj (:states particles)
                         (first (:retained particles)))
            ancestries (into [] (*map* conj
                                       (:ancestries particles)
                                       states))
            retained (rest (:retained particles))]
        (assoc particles
          :ancestries ancestries
          :retained retained)))))

(defnp run-observe
  [particles line]
  "Runs a observe statement on a particle set, printing
  the return values to *predict*, and discarding any
  changes to the execution states."
  (let [; :interpretation is of the form (erp-name arg arg arg ... value)
        ; TODO this is retarded and we should fix it
        expr (nth (:interpretation line) 1)
        value  (nth (:interpretation line) 2)
        n (:line line)
        states (into []
                     (*map* (fn [s]
                              (-> s
                                  (enter-expr n)
                                  (observe expr value)
                                  first
                                  (exit-expr)))
                            (:states particles)))]
    (resample (assoc particles :states states))))

(defnp run-predict
  [particles line]
  "Runs a predict statement on a particle set, printing
  the return values to *predict*, and discarding any
  changes to the execution states."
  (if *retained-only*
    ; run predict for retained
    (smc/run-predict (assoc particles
                       :states [(peek (peek (:ancestries particles)))])
                     line)
    ; run predicts for full particle set
    (let [; check whether we need to include retained-state
          retained-state (if (some? (:retained particles))
                           (peek (peek (:ancestries particles))))
          states (if (some? retained-state)
                   ; we're doing cond-smc, retained particle
                   ; is the last element of the last ancestor stack
                   (conj (:states particles) retained-state)
                   ; we're doing smc
                   (:states particles))]
      (smc/run-predict (assoc particles :states states) line)))
    particles)

(def run-methods
  {:assume run-assume
   :observe run-observe
   :predict run-predict})

(defnp run-line* [particles line]
  (let [run-fn (run-methods (keyword (:command line)))
        particles (run-fn particles line)]
    particles))


(defnp resample*
  [particles]
  "Resample a particle set. Performs cond-smc resampling when
  there is a retained particle, normal smc resampling when no
  retained particle exists."
  (if (nil? (:retained particles))
    ; do smc resample, but keep ancestries so we can choose
    ; a retained particle later
    (let [ancestries (into [] (*map* conj
                                     (:ancestries particles)
                                     (:states particles)))]
      (-> particles
          smc/resample*
          (assoc :ancestries ancestries)))
    ; do csmc resample
    (let [; get L particle states (including retained)
          states (conj (:states particles)
                       (first (:retained particles)))
          ; push current particle states onto L ancestor stacks
          ancestries (into [] (*map* conj
                                     (:ancestries particles)
                                     states))
          ; get log weights
          log-w (*map* log-weight states)
          ; pop retained continuation
          retained (rest (:retained particles))
          ; sample L-1 ancestor indices according to weight
          as (*resample-method* (math/cdf-exp log-w)
                                (count (:states particles)))
          ; update L-1 ancestries, add in retained ancestry
          ancestries (conj (into [] (map ancestries as))
                           (peek ancestries))
          ; set log-weight to log 1/L
          log-w (if (some isfinite? log-w)
                  (* -1.0 (log (count states)))
                  Double/NEGATIVE_INFINITY)
          ; update states
          states (into [] (map #(assoc (nth states %)
                                  :log-weight log-w :log-prop 0.0)
                               as))]
      (ParticleSet. states ancestries retained))))

(def sampler-methods
  {:run-line run-line*
   :resample resample*})

(extend ParticleSet
  ImportanceSampler
  sampler-methods)

(defn sample-retained [particles]
  (let [ancestries (:ancestries particles)
        log-w (map (comp log-weight peek)
                   ancestries)
        r (sample-log-weights log-w)]
    (seq (nth ancestries r))))

(defnp run-cond-smc
  [code num-part retained]
  (let [num-states (if (some? retained) (dec num-part) num-part)
        states (vec (repeat num-states
                            (reset-time empty-state)))
        ancestries (vec (repeat num-part []))
        retained (if (some? retained) (*map* reset-time retained))]
    (loop [particles (ParticleSet. states ancestries retained)
           code code]
      (if (empty? code)
          particles
          (recur (run-line particles (first code)) (rest code))))))

(defnp run-smc
  [code num-part]
  (run-cond-smc code num-part nil))

(defn run [code num-part num-sweep]
  (loop [; run conditional SMC with nil retained particle,
         ; which is equivalent to SMC run with L-1 particles
         particles (run-smc code num-part)
         ; select retained particle
         retained (sample-retained particles)
         ; index of sweep
         n 1]
    (if (some? *sample-count*)
        (set! *sample-count* (* n num-part)))
    (if (>= n num-sweep)
      particles
      (let [; resample particle with conditional smc
            particles (run-cond-smc code num-part retained)
            ; resample retained particle
            retained (sample-retained particles)]
        (recur particles retained (inc n))))))

(defn prun [& args]
  "Same as run, but uses pmap instead of map to execute statements in parallel"
  (binding [*map* clj/pmap]
    (clj/apply run args)))
