(ns anglican.sampler.ardb
  (:refer-clojure :exclude [eval apply rand rand-nth])
  (:require [clojure.core :as clj]
            [clojure.pprint :refer [pprint]]
			[clojure.test :refer [deftest testing is]]
            [clojure.set :as set]
            [clojure.algo.monads :refer [with-monad domonad m-map state-m]]

            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [slingshot.slingshot :refer [throw+]]

            [anglican.math :refer [log sqrt exp abs rand rand-nth]]
            [anglican.interpreter.base :as base
               :refer [sub-eval sample observe]]
            [anglican.sampler :refer [*predict* *sample-count* *output-predict*]]
            [anglican.interpreter.ardb :as ardb]
            [anglican.transform :refer [transform traverse]]))

;;; Adaptive RDB data and functions

;; Parameters --- as much as I dislike the use of dynamic global variables
;; for parameters, do I have a better choice?

(def ^:dynamic *reward-concentration*
  "applied to rewards to backpropagate the evidence"
  0.5)

(def ^:dynamic *backlog-length*
  "the length of random choice backlog"
  10)

(def ^:dynamic *exploration-factor*
  "UCB exploration factor"
  1.0)

(def ^:dynamic *probability-estimate*
  "predict probability estimate, :normal or :multinomial"
  :normal)

(def ^:dynamic *log-utility*
  "If true the utility is logarithmic in sample probability"
  true)

(def ^:dynamic *reward-policy*
  "One of:
   :utility --- the reward is the utility
   :difference --- the reward is the difference between
                   the utilities of two last samples
   :increase --- the reward is the increase
                 in the utility between two last samples
   :indicator --- 1 when the new utility is higher than the old,
                  otherwise 0"
  :increase)

(def ^:dynamic *random-choice-projection*
  "One of:
     - :identity (map to the addresses)
     - :shallow (address prefix until first application)
     - :deep (application index propagated through meta-data)"
  :deep)

(def ^:dynamic *training-sweeps*
  "Number of sweeps to train the weights, or nil for ethernal training"
  nil)

(def ^:dynamic *print-adaptive-state*
  "When true, the adaptive state is printed to the standard error after each sweep"
  false)

(def ^:dynamic *print-final-adaptive-state*
  "When true, the adaptive state is printed to the standard error
  at the end of the algorithm"
  true)

;; end of configuration parameters

(def ^:dynamic *training*
  "when true the random choice weights are updated from predicts"
  true)

;; Adaptive RDB state: random choice weights, predict density estimates,
;; and other things which help select random choices adaptively

(defrecord AdaptiveState
    [random-choice-rewards            ; a map of random choice (reward,count) by index
     random-choice-counts             ; counts of random choices per address, for statistics
     total-count                      ; total count of random choices
     random-choice-addr               ; random-choice-addr
     random-choice-indices            ; the backlog of random choices
     predict-distributions            ; a map of predict belief distributions by index
     last-predicts])                  ; a map of last predict values

(def ^:private prior-random-choice-reward-and-count
  "reward and count for unexplored random choice"
  [1 0])

(defn ^:private prior-predict-distribution
  "predict distribution before the first predict"
  []
  (case *probability-estimate*
    :normal [1. 1. 0.]
    :multinomial [{} 0.]))

(def ^:private initial-adaptive-state
  "initial adaptive state"
  (map->AdaptiveState {:random-choice-rewards {}
                       :random-choice-counts {}
                       :total-count 0
                       :random-choice-addr nil
                       :random-choice-indices '()
                       :predict-distributions {}
                       :last-predicts {}}))

;; decayed increment, both for rewards and for predicts
(defn update-mean
  "increments accumulated `mean' over `count' samples
  with updated value `x' with `weight' (which may be less than 1)"
  ([mean count x] (update-mean mean count x 1.))
  ([mean count x weight] (/ (+ (* count mean) (* weight x)) (+ count weight))))

;; Projections for the weighing of random choices

(defn ^:private addr-until-apply
  "returns the address prefix until the first application,
  used as a projection of random choice addresses into
  a trace-invariant space"
  [addr]
  (take-while #(not= 0 %) addr))

(defn ^:private compute-random-choice-index
  "returns random choice index for a random choice,
  the exact way in which the index is calculated depends
  on the abstraction chosen"
  [random-choice-addr random-choice]
  (case *random-choice-projection*
    :identity random-choice-addr
    :shallow (addr-until-apply random-choice-addr)
    :deep  (:application-index (meta (:proc random-choice)) 0)))

(defn ^:private random-choice-reward-and-count
  "returns random choice reward and count for a choice"
  [adaptive-state random-choice-index]
  (get (:random-choice-rewards adaptive-state) random-choice-index
       prior-random-choice-reward-and-count))

(defn ^:private backpropagate-reward
  "backpropagates the reward to recent choices"
  [adaptive-state new-reward]
  (loop [adaptive-state adaptive-state
         indices (:random-choice-indices adaptive-state)
         remaining-weight 1.]
    (if (some? indices)
      (let [[index & indices] indices
            weight (* remaining-weight *reward-concentration*)]
        (recur
         (update-in adaptive-state [:random-choice-rewards index]
                    (fnil (fn [[reward count]]
                            [(update-mean reward count new-reward weight)
                             (+ count weight)])
                          prior-random-choice-reward-and-count))
         indices
         (- remaining-weight weight)))
      adaptive-state)))

(defn ^:private update-random-choice-rewards
  "updates random choice weight and count based on the reward"
  [adaptive-state new-reward]
  (-> adaptive-state
      (backpropagate-reward new-reward)
      (update-in [:total-count] inc)))

(defn ^:private predict-index
  "returns predict index for the predict"
  [predict]
  (:line (meta predict)))

(defn ^:private predict-value
  "returns predict value for the predict"
  [predict]
  (second predict))

;; erf estimate to compute normal cdf

(def ^:private erf
  "ERF, the error function, approximation according to
  A&S 7.1.26 (http://people.math.sfu.ca/~cbm/aands/)"
  (let [a1 0.254829592
        a2 -0.284496736
        a3 1.421413741
        a4 -1.453152027
        a5 1.061405429
        p 0.3275911]
    (fn [x]
      (let [y (/ 1. (+ 1. (* p (abs x))))
            z (- 1.
                 (* y (exp (- (* x x)))
                    (+ a1 (* y (+ a2 (* y (+ a3 (* y (+ a4 (* y a5))))))))))]
        (if (> x 0.) z (- z))))))

(let [>0 #(max Float/MIN_VALUE %)
      >0<1 #(max Float/MIN_VALUE (min 1. %))]

  (defn ^:private predict-probability
    "returns predict probability computation according to the current belief"
    [adaptive-state predict-index]
    (let [distribution (get (:predict-distributions adaptive-state) predict-index (prior-predict-distribution))]

      ;; I want to be simple here, this is just a weighing model,
      ;; everything that is not off too much will work.

      (case *probability-estimate*
        :normal
        (let [[mean_x mean_x2 count] distribution]
          (if (< count 2) (fn [_] (>0<1 0.))
              (let [mu mean_x
                    sigma (sqrt (* (/ count
                                      (- count 1.))
                                   (>0 (* mean_x mean_x))))]
                (fn [predict]
                  ;; the probability
                  (>0<1 (erf (/ (abs (- predict mu)) sigma 1.4142136)))))))
        
        :multinomial
        (let [[counts total-count] distribution]
          (if (< total-count 2) (fn [_] (>0<1 0.))
              (fn [predict]
                (>0<1 (/ (counts predict 0.) total-count))))))))

  (defn ^:private predict-utility
    "returns utility for a predict"
    [adaptive-state predict-index predict-value]
    (let [prob (if (number? predict-value)         ; probability if number, otherwise just give uniform reward
                 ((predict-probability adaptive-state predict-index) predict-value)
                 1.)]
      (if *log-utility* (- (log (>0 (- 1. prob)))) prob))))

(defn ^:private predict-reward
  "predict reward"
  [adaptive-state predict-index predict-value]
  (let [utility (predict-utility adaptive-state predict-index predict-value)
        prev-value ((:last-predicts adaptive-state) predict-index)
        prev-utility (if (some? prev-value)
                       (predict-utility adaptive-state predict-index prev-value)
                       utility)]
    (case *reward-policy*
      :utility utility
      :difference (abs (- utility prev-utility))
      :increase (max 0. (- utility prev-utility))
      :indicator (if (> utility prev-utility) 1. 0.))))

(defn ^:private update-predict-distribution
  "updates belief distribution for a given index,
  and returns the update adaptive state"
  [adaptive-state predict-index predict-value]
  (if (number? predict-value)
    (update-in adaptive-state [:predict-distributions predict-index]
               (fnil (fn [distribution]
                       (case *probability-estimate*
                         :normal
                         (let [[mean_x mean_x2 count] distribution]
                           [(update-mean mean_x count predict-value)
                            (update-mean mean_x2 count (* predict-value predict-value))
                            (inc count)])

                         :multinomial
                         (let [[counts total-count] distribution]
                           [(update-in counts [predict-value] (fnil inc 0.))
                            (inc total-count)])))
                     (prior-predict-distribution)))
    adaptive-state))

;;; RDB (modified)

(declare run-assume run-observe run-predict)

(defn run-assume
  [state line]
  "Runs an assume statement on the state, returning an updated state."
  (let [expr (:interpretation line)
        n (:line line)]
    (first (sub-eval state expr n))))

(defn run-predict
  [state line]
  "Runs a predict statement on the state, printing
  the return values to *predict*, and discarding any
  changes to the execution state."
  ;; in RDB this actually stores predicts in the state
  ;; instead of printing them, the predicts are printed
  ;; in `run' by calling to *output-predict*
  (let [expr (:interpretation line)
        n (:line line)]
    ; store y
    (update-in state [:predicts] (fnil conj [])
               ;; Add meta data specifying the source code location
               ;; of the predict; used to compute the influence
               (with-meta (list expr (second (sub-eval state expr n)))
                 {:line n}))))

(defn run-observe
  [state line]
  "Runs an observe statement on the state, returning an updated set."
  (let [;; :interpretation is of the form (erp-name arg arg arg ... value)
        ;; TODO this is retarded and we should fix it
        expr (nth (:interpretation line) 1)
        value  (nth (:interpretation line) 2)
        n (:line line)
        state (-> state
                  (base/enter-expr n)
                  (observe expr value)
                  first
                  (base/exit-expr))]
    state))

(def run-methods
  {:assume run-assume
   :observe run-observe
   :predict run-predict})

(defn run-line* [particles line]
  (let [run-fn (run-methods (keyword (:command line)))]
    (run-fn particles line)))

; Should we extend it somehow?

(defn resample* [] nil)

(def sampler-methods
  {:run-line run-line*
   :resample resample*})

(defn run-code
  [code state]
   (loop [state state
          lines code]
     (if (empty? lines)
       state
       (recur (run-line* state (first lines))
              (rest lines)))))

; Where to put it?
(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn get-logprob
  [state key]
  (let
    [value (get-in state [:system :logprobabilities key])]
    (if (nil? value) 0 value)))

;; ARDB weighted random choice

(defn random-choice-addr-weight-map
  "builds a map of address rewards"
  [state adaptive-state]
  (into {} (map (fn [[addr choice]]
                  (let [index (compute-random-choice-index addr choice)
                        [reward count] (random-choice-reward-and-count adaptive-state index)]
                    ;; UCB weight
                    [addr (if (< (:total-count adaptive-state) 100) 1
                              (+ reward (sqrt (* *exploration-factor*
                                                 (/ (log (:total-count adaptive-state))
                                                    count)))))]))
                (merge (get-in state [:rand-state :sampled]) (get-in state [:rand-state :reused])))))

(defn random-choice-probability
  "computes the probability of a random choice"
  [state adaptive-state]
  (let [addr-weight-map (random-choice-addr-weight-map state adaptive-state)]
    (- (log (addr-weight-map (:random-choice-addr adaptive-state)))
       (log (reduce + (vals addr-weight-map))))))

(defn select-random-choice-addr
  "selects random choice address randomly with probability proportional to the reward"
  [state adaptive-state]
  (let [addr-weight-map (random-choice-addr-weight-map state adaptive-state)
        fortune (rand (reduce + (vals addr-weight-map)))]
    (loop [running-weight 0
           addr-weights (seq addr-weight-map)]
      (let [[[addr weight] & addr-weights] addr-weights
            running-weight (+ running-weight weight)]
        (if (>= running-weight fortune) addr
            (recur running-weight addr-weights))))))

(defn get-MH-acceptance-ratio
  [adaptive-state old-state new-state old-value-prob new-value-prob]
  (let [logP (fn [state] (+ (get-logprob state :spent-probability)
                            (get-logprob state :observed-probability)
                            (get-logprob state :restored-probability)))
        logP-old (logP old-state)
        logP-new (logP new-state)

        new-random-choice-probability (random-choice-probability new-state adaptive-state)
        old-random-choice-probability (random-choice-probability old-state adaptive-state)

        ;; logQ-randomness cancels out in the final fraction
        logQ-randomness (fn [state] (get-logprob state :spent-probability))
        logQ-old-randomness (logQ-randomness old-state)
        logQ-new-randomness (logQ-randomness new-state)

        logQ-old-to-new (+ old-random-choice-probability new-value-prob logQ-new-randomness)
        logQ-new-to-old (+ new-random-choice-probability old-value-prob logQ-old-randomness)]

    (- (+ logP-new logQ-new-to-old) (+ logP-old logQ-old-to-new))))

(defn accept-MH-transition?
  [adaptive-state old-state new-state old-value-prob new-value-prob]
  (let
    [MH-acceptance-ratio (get-MH-acceptance-ratio adaptive-state old-state new-state old-value-prob new-value-prob)
     random-value (log (rand 0.0 1.0))
     decision (> MH-acceptance-ratio random-value)]
    decision))

(defn update-adaptive-state
  "updates the adaptive state from the current predicts"
  [adaptive-state predicts]
  (loop [predicts predicts
         reward 0.
         number-of-predicts 0
         adaptive-state adaptive-state]
    (if (some? predicts)
      (let [[predict & predicts] predicts]
        (let [predict-index (predict-index predict)
              predict-value (predict-value predict)]
          (recur predicts
                 (+ reward (predict-reward adaptive-state predict-index predict-value))
                 (inc number-of-predicts)
                 (-> adaptive-state
                     (update-predict-distribution predict-index predict-value)
                     (assoc-in [:last-predicts predict-index] predict-value)))))
      (if (and *training* (> number-of-predicts 0))
        (update-random-choice-rewards adaptive-state (/ reward number-of-predicts))
        adaptive-state))))

(defn run-MH
  ([code state]
     (if (empty? (keys (:sampled (:rand-state state))))
       (run-code code (base/reset-time ardb/empty-state))
       (let [random-choice-addr (select-random-choice-addr state (:adaptive state))
             random-choice (get (:sampled (:rand-state state)) random-choice-addr)
             random-choice-index (compute-random-choice-index random-choice-addr random-choice)

             ;; resample the selected random choice
             new-value (second (sample state (:proc random-choice) (:args random-choice)))
             new-value-prob (ardb/logprob-from-sample state new-value)

             ;; carry over new selection to the adaptive state
             adaptive-state (-> (:adaptive state)
                                (assoc :random-choice-addr random-choice-addr)
                                (update-in [:random-choice-indices]
                                           (fn [indices]
                                             (take *backlog-length* (cons random-choice-index indices))))
                                (update-in [:random-choice-counts random-choice-index] (fnil inc 0)))

             ;; execute the new
             new-state (run-code code
                                 (-> (base/reset-time ardb/empty-state)
                                     (assoc-in [:rand-state :cached] (:sampled (:rand-state state)))
                                     (assoc-in [:rand-state :cached random-choice-addr] new-value)))

             ;; rerun the old inference to compute conditional p(x\x') probability of resampled choices
             ;; [I don't understand fully this magic --- some one needs to guide me through]
             old-value-prob (ardb/logprob-from-sample new-state random-choice)
             cached-minus-reused
             (set/difference
              (set (keys (get-in new-state [:rand-state :cached])))
              (set (keys (get-in new-state [:rand-state :reused]))))
             old-state (run-code code
                                 (-> (base/reset-time ardb/empty-state)
                                     (assoc-in [:rand-state :cached] (:sampled (:rand-state state)))
                                     (assoc-in [:system :consider-as-new] cached-minus-reused)))

             state (if (accept-MH-transition? adaptive-state old-state new-state old-value-prob new-value-prob)
                     (-> new-state

                         (assoc-in [:rand-state :sampled]
                                   (merge (get-in new-state [:rand-state :sampled])
                                          (get-in new-state [:rand-state :reused])))
                         (dissoc-in [:rand-state :reused])

                         (dissoc-in [:rand-state :cached])

                         (dissoc-in [:system :logprobabilities])
                         (dissoc-in [:system :consider-as-new]))
                     state)

             ;; update the adaptive state based on the predicts
             adaptive-state (update-adaptive-state adaptive-state (:predicts state))

             ;; attach adaptive-state with the new choice to the new state
             state (assoc state :adaptive adaptive-state)]
         state)))

  ([code state times]
     (loop [state state
            remain-MH-iterations times]
       (if (> remain-MH-iterations 0)
         (recur
          (run-MH code state)
          (dec remain-MH-iterations))
         state))))

;; Source to source transformer that adds meta data to the random
;; sampling. The meta data is then used to project random choices to
;; their source locations.

(def ^:private tx-decorate {:type :decorate
                            :application-index 1}) ; 0 is reserved for unknown locations
(defmethod transform [:application :decorate] [[operator & operands :as code] tag tx]
    [(with-meta code (merge (meta code) {:application-index (:application-index tx)}))
     (update-in tx [:application-index] inc)])

(defn ^:private decorate
  "decorates the code with meta-data"
  [code]
  (vec
   (first
    ((with-monad state-m
       (m-map (fn [form]
                (domonad
                 [interpretation #(traverse (:interpretation form) %)]
                 (assoc form :interpretation interpretation)))
              code))
     tx-decorate))))

(defn ^:private print-adaptive-state
  "pretty-prints adaptive state to the standard error"
  [state]
  (binding [*out* *err*]
    (pprint (:adaptive state))))

(defn run
  "Runs MH on code."
  [code num-MH-iterations-per-sweep num-sweeps]
  (let [code (decorate code)]
    (binding [*training* *training*]
      (loop [state (assoc
                     (run-code code (base/reset-time ardb/empty-state))
                     :adaptive initial-adaptive-state)
             remain-sweeps num-sweeps
             sweep-id 0]

        (if (> remain-sweeps 0)
          (do
            (when *print-adaptive-state*
              (print-adaptive-state state))

            (when (some? *sample-count*)
              (set! *sample-count* sweep-id))

            ;; output the predicts
            (doseq [predict (:predicts state)]
              (clj/apply *output-predict* (if (some? *sample-count*)
                                            (concat predict [*sample-count*])
                                            predict)))

            ;; recur cannot be nested inside binding,
            ;; set! is the simplest way
            (when (= sweep-id *training-sweeps*)
              ;; switch to non-adaptive weighted mode
              (set! *training* false))

            (recur
             (run-MH code state num-MH-iterations-per-sweep)
             (dec remain-sweeps)
             (inc sweep-id)))

          (when *print-final-adaptive-state*
            (print-adaptive-state state)))))))

(deftest test-addr-until-apply
  (testing "addr-until-apply"
    (is (= (addr-until-apply [:global :3 1 2 0 2 0 1 1]) [:global :3 1 2])
        "address includes application")
    (is (= (addr-until-apply [:global :2 3 1]) [:global :2 3 1])
        "address does not include application")))
