(ns anglican.sampler.cascade
  (:refer-clojure :exclude [eval apply])
  (:require [clojure.core :as clj]
            [clojure.data.priority-map :refer [priority-map]]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [slingshot.slingshot :refer [try+ throw+]]
            [anglican.diag :refer [intercepting-errors]]
            [anglican.parser :as parser]
            [anglican.interpreter.base :as base
               :refer [Interpreter eval sub-eval apply observe dbg]]
            [anglican.math :as math
             :refer [log exp mean isfinite? log-sum-exp floor ceil]]
            [anglican.interpreter.smc :as smc
             :refer [log-weight]]
            [anglican.sampler
             :refer [*predict* *sample-count* *start-time* *output-predict*]])
  (:import [java.util.concurrent Executors]))

(declare run-assume run-observe queue-predict run-queued-predicts)

(defrecord Particle [id state predict-exprs line-number remaining-forks multiplicity])

(defn async-resample! [particle checkpoint num-initial-particles]
  (let [w (exp (log-weight (:state particle)))
        cp @checkpoint
        k (or (:count cp) 0)
        W-avg (/ (+ (or (:sum-weight cp) 0.0) w)
                 (inc k))
        M-total (:forks cp)
        R (if (> W-avg 0.0) (/ w W-avg) 0.0)]
    (let [[M V] (if (<= R 1.0)
                  (if (< (rand) R)
                    [1 W-avg]
                    [0 0.0])
                  (if (> M-total (min k num-initial-particles))
                    [(int (floor R)) (/ w (floor R))]
                    [(int (ceil R)) (/ w (ceil R))]))
          state (assoc (:state particle) :log-weight (log V))]
          (swap! checkpoint (fn [cp]
                             ; REVIEW: could this lead to underflow issues?
                             {:sum-weight (+ (:sum-weight cp) w)
                              :count (inc (:count cp))
                              :forks (+ (:forks cp) M)}))
          (assoc particle
            :remaining-forks (dec M)
            :state state))))

(defn run-next-line [particle code]
  (let [statement (nth code (:line-number particle))
        directive (:command statement)
        run-fn (case directive
                 "assume" run-assume
                 "predict" queue-predict
                 "observe" run-observe)
        particle (run-fn particle statement)]
    (update-in particle [:line-number] inc)))


(defn run-assume
  [particle statement]
  (let [expr (:interpretation statement)
        addr (:line statement)
        state (first (sub-eval (:state particle) expr addr))]
    (assoc particle :state state)))

(defn run-observe
  [particle statement]
  (let [; :interpretation has form
        ; (observe expr value)
        expr (nth (:interpretation statement) 1)
        value  (nth (:interpretation statement) 2)
        addr (:line statement)
        state (-> (:state particle) ; threading the state
                  (base/enter-expr addr)
                  (-> (observe expr value)
                      first)
                  (base/exit-expr))]
      (assoc particle :state state)))

(defn queue-predict
  [particle statement]
  (let [expr (:interpretation statement)]
    (update-in particle [:predict-exprs] conj expr)))

(defn run-queued-predicts
  ([particle sample-count]
   (let [state (:state particle)
         lw (log-weight state)
         exprs (:predict-exprs particle)
         predicts (map (fn [e]
                         [e (second (eval state e))])
                       exprs)]
     (doseq [[e v] predicts]
       (if (some? sample-count)
         (*output-predict* e v lw sample-count)
         (*output-predict* e v lw)))))
  ([particle] (run-queued-predicts particle nil)))


(def initial-particle
  (map->Particle {:id 0
                  :state smc/empty-state
                  :predict-exprs []
                  :line-number 0
                  :remaining-forks 0
                  :multiplicity 1})) ; ignored for now

(def clone-particle
  (let [particle-count (atom 0)]
     (fn [particle]
       (assoc particle :id (swap! particle-count inc)))))

(defn insert-particle!
  "insert a particle into the pool"
  [particle-pool particle]
  (alter particle-pool conj particle))

(defn initialize-pool!
  "insert initial particles into particle pool"
  [particle-pool num-initial-particles]
  (dosync
    (dotimes [_ num-initial-particles]
      (insert-particle! particle-pool (clone-particle initial-particle)))))

(defn fetch-particle!
  "fetch a particle from the pool"
  [particle-pool]
  (dosync
   (if (< (rand) (/ 1. (inc (count @particle-pool))))
     ; create a new particle
     (clone-particle initial-particle)
     ; checkout particle
     (let [ix (rand-int (count @particle-pool))
           particle (nth @particle-pool ix)]
       (alter particle-pool (fn [v] (vec (concat (subvec v 0 ix) (subvec v (inc ix))))))
       particle))))

(defn cascade [code particle-pool checkpoints sample-count num-samples num-initial-particles]
  (while (< @sample-count num-samples)
    (let [particle (fetch-particle! particle-pool)]
      (when (> (:remaining-forks particle) 0)
        (dosync
         (insert-particle! particle-pool
                           (update-in particle [:remaining-forks] dec))))
      (loop [particle particle]
        ; run next line in code
        (let [statement (nth code (:line-number particle))
              particle (run-next-line particle code)]
          (if (= (:line-number particle) (count code))
            ; execute predict statements
            (let [current-sample-index (dec (swap! sample-count inc))]
              (if (some? *sample-count*)
                (run-queued-predicts particle current-sample-index)
                (run-queued-predicts particle)))
            (if (= (:command statement) "observe")
              ; determine number of forks, update average weight
              (let [particle (async-resample! particle
                                              (get checkpoints (:line statement))
                                              num-initial-particles)]
                ; put particle back into queue when number of descendants is at least one
                (when (> (:remaining-forks particle) -1)
                  (dosync
                   (insert-particle! particle-pool particle))))
              ; move on to next line
              (recur particle))))))))

(defn run
  ([code num-samples num-threads num-initial-particles debug]
   (let [particle-pool (ref [])
         checkpoints (into {} (keep (fn [s] (when (= (:command s) "observe")
                                              [(:line s) (atom {:sum-weight 0.0 :count 0 :forks 0})]))
                                    code))
         sample-count (atom 0)]
     (initialize-pool! particle-pool num-initial-particles)
     (let [bindings (get-thread-bindings)]
       (dotimes [_ num-threads]
         (.start (Thread. (fn []
                            (intercepting-errors
                              [:debug debug]
                              (with-bindings bindings
                                (cascade code
                                         particle-pool
                                         checkpoints
                                         sample-count
                                         num-samples
                                         num-initial-particles)))))))))))
