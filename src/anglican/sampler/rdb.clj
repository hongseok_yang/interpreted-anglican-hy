(ns anglican.sampler.rdb
  (:refer-clojure :exclude [eval apply rand rand-int])
  (:require [clojure.core :as clj]
            [clojure.set]
            [taoensso.timbre.profiling :as profiling
             :refer (pspy pspy* profile defnp p p*)]
            [anglican.parser :as parser]
            [anglican.math :refer [log exp rand rand-int]]
            [anglican.interpreter.base :as base
               :refer [Interpreter eval sub-eval apply sample observe]]
            [anglican.interpreter.rdb :as rdb]
            [anglican.sampler :refer [*predict* *sample-count* *map* *output-predict*]]
            [slingshot.slingshot :refer [throw+]]))

(declare run-assume run-observe run-predict)

(defnp run-assume
  [state line]
   "Runs an assume statement on the state, returning an updated state."
  (let [expr (:interpretation line)
        n (:line line)]
    (first (sub-eval state expr n))))

(defnp run-predict
  [state line]
   "Runs a predict statement on the state, printing
  the return values to *predict*, and discarding any
  changes to the execution state."
  (let [expr (:interpretation line)
        n (:line line)]
    (update-in state [:predicts] (fnil conj []) (list expr (second (sub-eval state expr n))))))

(defnp run-observe
  [state line]
  "Runs an observe statement on the state, returning an updated set."
  (let [; :interpretation is of the form (erp-name arg arg arg ... value)
        ; TODO this is retarded and we should fix it
        expr (nth (:interpretation line) 1)
        value  (nth (:interpretation line) 2)
        n (:line line)
        state (-> state
                (base/enter-expr n)
                (observe expr value)
                first
                (base/exit-expr))]
    state))

(def run-methods
  {:assume run-assume
   :observe run-observe
   :predict run-predict})

(defn run-line* [particles line]
  (let [run-fn (run-methods (keyword (:command line)))]
    (run-fn particles line)))

; Should we extend it somehow?

(defn resample* [] nil)

(def sampler-methods
  {:run-line run-line*
   :resample resample*})

(defnp run-code
  [code state]
   (loop [state state
          lines code]
     (if (empty? lines)
       state
       (recur (run-line* state (first lines))
              (rest lines)))))

; Where to put it?
(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))

(defn get-logprob
  [state key]
  (let
    [value (get-in state [:system :logprobabilities key])]
    (if (nil? value) 0 value)))

(defn get-MH-acceptance-ratio
  [old-state new-state old-value-prob new-value-prob]
  (let
    [logP-old (+ (get-logprob old-state :spent-probability) (get-logprob old-state :observed-probability) (get-logprob old-state :restored-probability))
     logP-new (+ (get-logprob new-state :spent-probability) (get-logprob new-state :observed-probability) (get-logprob new-state :restored-probability))
     old-random-choice-logprob (- 0 (log (+ (count (get-in old-state [:rand-state :sampled])) (count (get-in old-state [:rand-state :reused])))))
     new-random-choice-logprob (- 0 (log (+ (count (get-in new-state [:rand-state :sampled])) (count (get-in new-state [:rand-state :reused])))))
     logQ-old-randomness (get-logprob old-state :spent-probability)
     logQ-new-randomness (get-logprob new-state :spent-probability)
     logQ-old-to-new (+ old-random-choice-logprob new-value-prob logQ-new-randomness)
     logQ-new-to-old (+ new-random-choice-logprob old-value-prob logQ-old-randomness)]
    (- (+ logP-new logQ-new-to-old) (+ logP-old logQ-old-to-new))))

(defnp accept-MH-transition?
  [old-state new-state old-value-prob new-value-prob]
  (let
    [MH-acceptance-ratio (get-MH-acceptance-ratio old-state new-state old-value-prob new-value-prob)
     random-value (log (rand 0.0 1.0))
     decision (if (> MH-acceptance-ratio random-value) true false)]
    decision))

(defn run-MH
  ([code state]
    (if (empty? (keys (:sampled (:rand-state state))))
      (run-code code (base/reset-time rdb/empty-state))
      (let
        [addresses-of-random-choices (keys (:sampled (:rand-state state)))
         random-choice-addr (nth addresses-of-random-choices
                            (rand-int 0 (count addresses-of-random-choices)))
         random-choice (get (:sampled (:rand-state state)) random-choice-addr)
         new-value (second (sample state (:proc random-choice) (:args random-choice)))
         new-value-prob (rdb/logprob-from-sample state new-value)
         new-state (assoc-in (base/reset-time rdb/empty-state) [:rand-state :cached] (:sampled (:rand-state state)))
         new-state (assoc-in new-state [:rand-state :cached random-choice-addr] new-value)
         new-state (run-code code new-state)
         old-value-prob (rdb/logprob-from-sample new-state random-choice)
         cached-minus-reused
           (clojure.set/difference
             (set (keys (get-in new-state [:rand-state :cached])))
             (set (keys (get-in new-state [:rand-state :reused]))))
         old-state-again
           (assoc-in (base/reset-time rdb/empty-state) [:rand-state :cached] (:sampled (:rand-state state)))
         old-state-again (assoc-in old-state-again [:system :consider-as-new] cached-minus-reused)
         old-state-again
           (run-code code old-state-again)]
        (if (accept-MH-transition? old-state-again new-state old-value-prob new-value-prob)
          (let
            [new-state
               (assoc-in new-state [:rand-state :sampled]
                         (merge (get-in new-state [:rand-state :sampled])
                                (get-in new-state [:rand-state :reused])))
             new-state (dissoc-in new-state [:rand-state :cached])
             new-state (dissoc-in new-state [:system :logprobabilities])
             new-state (dissoc-in new-state [:system :consider-as-new])]
            new-state)
          state))))
      ([code state times]
      (loop [state state
             remain-MH-iterations times]
        (if (> remain-MH-iterations 0)
          (recur
            (run-MH code state)
            (dec remain-MH-iterations))
          state))))

(defnp run
  "Runs MH on code."
  [code num-MH-iterations-per-sweep num-sweeps]
    (let
      [empty-state (base/reset-time rdb/empty-state)
       prior-state (run-code code empty-state)]
    (loop [state prior-state
           remain-sweeps num-sweeps
           sweep-id 0]
      (if (> remain-sweeps 0)
        (do
          (if (some? *sample-count*)
            (set! *sample-count* sweep-id))
          (doall
            (map (fn [y] (clj/apply *output-predict* (if (some? *sample-count*) 
                                                       (concat y [*sample-count*])
                                                       y)))
                 (get-in state [:predicts])))
          (let
            [new-state (run-MH code state num-MH-iterations-per-sweep)]
            (recur
              new-state
              (dec remain-sweeps)
              (inc sweep-id))))))))
