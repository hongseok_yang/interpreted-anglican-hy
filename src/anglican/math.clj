(ns anglican.math
  (:refer-clojure :exclude [rand rand-int rand-nth])
  (:import [cern.jet.random.engine MersenneTwister]))

(def random-seed (atom 3))
(defn set-random-seed! [s] (reset! random-seed s))
(def random (atom (new java.util.Random @random-seed)))

(def mersenne-seed (atom 7))
(defn set-mersenne-seed! [s] (reset! mersenne-seed s))
(def mersenne-twister (atom (new MersenneTwister @mersenne-seed)))

; wrap some primitives from java.lang.Math so they can be used wtih
; clojure primitives such as map and reduce
(defn abs [x] (Math/abs x))
(defn floor [x] (Math/floor x))
(defn ceil [x] (Math/ceil x))
(defn round [x] (Math/round x))
(defn rint [x] (Math/rint x))
(defn signum [x] (Math/signum x))

(defn sin [x] (Math/sin x))
(defn cos [x] (Math/cos x))
(defn tan [x] (Math/tan x))
(defn asin [x] (Math/asin x))
(defn acos [x] (Math/acos x))
(defn atan [x] (Math/atan x))
(defn sinh [x] (Math/sinh x))
(defn cosh [x] (Math/cosh x))
(defn tanh [x] (Math/tanh x))

(defn log [x] (Math/log x))
(defn exp [x] (Math/exp x))
(defn cbrt [x] (Math/cbrt x))
(defn sqrt [x] (Math/sqrt x))
(defn pow [a b] (Math/pow a b))

(defn reset-rng!
  "Resets all random number generators to their default states"
  ([]
    (reset! random (new java.util.Random @random-seed))
    (reset! mersenne-twister (new MersenneTwister @mersenne-seed)) nil)
  ([seed]
   (set-random-seed! seed)
   (set-mersenne-seed! seed)
   (reset-rng!)))

(defn isnan? [dbl]
  (Double/isNaN dbl))

(defn isfinite? [dbl]
  (not (or (Double/isNaN dbl) (Double/isInfinite dbl))))

(defn mean
  [coll]
  (/ (reduce + 0.0 coll) (count coll)))

(defn var
  [coll]
  (let [E-x (mean coll)
        E-x2 (/ (reduce (fn [s v] (+ s (* v v))) 0.0 coll) (count coll))]
    (- E-x2 (* E-x E-x))))

(defn cumsum
   "With one arg x returns lazy cumulative sum sequence y with (= (nth y) (cumsum x))
    With two args t, x returns lazy cumulative sum sequence y with (= (nth y) (+ t (cumsum x)))"
  ([x] (cumsum 0 x))
  ([t x] (if (empty? x) ()
           (let [y (+ t (first x))]
             (cons y (lazy-seq (cumsum y (rest x))))))))

(defn sum
  [coll]
  (reduce + 0.0 coll))

(defn normalize [values]
  (let [Z (reduce + values)]
    (map #(/ % Z) values)))

(defn log-sum-exp [log-values]
  (let [vmax (reduce max 0.0 log-values)
        lv0 (map #(- % vmax) log-values)]
    (+ vmax (log (reduce + 0.0 (map exp lv0))))))

(defn log-normalize [log-values]
  (let [log-Z (log-sum-exp log-values)]
    (map #(- % log-Z) log-values)))

(defn cdf
  [weights]
  (let [z (cumsum weights)
        Z (last z)]
    (if (= 1.0 (double Z))
      z
      (mapv #(/ % Z) z))))

(defn pdf-exp [log-weights]
  (let [m (reduce max log-weights)
        p0 (map #(exp (- % m)) log-weights)
        Z (reduce + p0)]
    (map #(/ % Z) p0)))

(defn cdf-exp
  [log-weights]
    (cumsum (pdf-exp log-weights)))

(defn eff-sample-size [log-weights]
  (/ 1.0 (reduce + (map #(* % %) (pdf-exp log-weights)))))

(def pi java.lang.Math/PI)

(defn gammaln
  "Computes ln(gamma(x))"
  [x] (. cern.jet.stat.Gamma logGamma x))

(defn rand
  "Returns a uniformly disributed random double"
  ([]
   (. @random nextDouble))
  ([max]
   (* max (rand)))
  ([min max]
   (+ min (* (rand) (- max min)))))

(defn randn
  "Returns a normally distributed random double"
  ([]
   (. @random nextGaussian))
  ([mean std]
   (+ mean
      (* std (randn)))))

(defn rand-int
  "Returns a uniformly distribited random integer between min (inclusive)
  and max (exclusive)"
  ([max]
   (rand-int 0 max))
  ([min max]
   (int (rand min max))))

(defn rand-nth
  "Return a random element of the (sequential) collection. Will have
  the same performance characteristics as nth for the given
  collection."
  [coll]
  (nth coll (rand-int (count coll))))

(defn value-counts [values]
  (reduce (fn [m v] (update-in m [v] (fnil inc 0))) {} values))

(defn sample-cdf
  [cdf]
  (let [r (rand)]
    (first (keep-indexed #(when (>= %2 r) %1) cdf))))

(defn sample-multinomial
  ([cdf n]
    (take n (repeatedly #(sample-cdf cdf))))
  ([cdf]
    (sample-multinomial cdf (count cdf))))

(defn sample-stratified
  ([cdf n]
  (let [N (count cdf)
        us (into [] (repeatedly n #(rand)))]
    (loop [l  0
           as []
           rs (map (partial * n) cdf)]
      (if (empty? rs)
        as
        (let [r (first rs)
              k (min (floor r) (dec n))
              O (min (floor (+ r (nth us k))) n)]
          (recur (inc l)
                 (into as (repeat (- O (count as)) l))
                 (rest rs)))))))
  ([cdf]
   (sample-stratified cdf (count cdf))))

(defn sample-systematic
  ([cdf n]
  (let [N (count cdf)
        u (rand)]
    (loop [l  0
           as []
           rs (map (partial * n) cdf)]
      (if (empty? rs)
        as
        (let [r (first rs)
              O (min (floor (+ r u)) n)]
          (recur (inc l)
                 (into as (repeat (- O (count as)) l))
                 (rest rs)))))))
  ([cdf]
   (sample-systematic cdf (count cdf))))

(defn sample-weights
  [weights]
  (sample-cdf (cdf weights)))

(defn sample-log-weights
  [log-weights]
  (sample-cdf (cdf-exp log-weights)))

(defn g-statistic
  [freqs lnpdf]
  (let [N (* 1.0 (reduce + (vals freqs)))
        emp-support (keys freqs)
        emp-probs (map (fn [c]
                         (/ c N))
                       (vals freqs))
        ; calculate log probability under theoretical lnpdf for each unique value
        log-probs (mapv lnpdf emp-support)]
    ; check that no sampled values lie outside support of the lnpdf
    (if (some (fn [lp] (= (log 0.0) lp)) log-probs)
      Double/POSITIVE_INFINITY
      (reduce + (map (fn [ep lp] (* 2 ep N (- (log ep) lp)))
                     emp-probs
                     log-probs)))))

(defn g-quantile
  [samples lnpdf]
  (let
    [freqs (frequencies samples)
     G (g-statistic freqs lnpdf)]
    ; evaluate cdf
    (. umontreal.iro.lecuyer.probdist.ChiSquareDist
       cdf
       ; degrees of freedom
       (dec (count freqs))
       ; decimal digits of precision
       10
       G)))

(defn chisq-statistic
  [freqs lnpdf]
  (let [N (* 1.0 (reduce + (vals freqs)))
        th-freqs (into {} (map (fn [[x c]] [c (* N (exp (lnpdf x)))])
                               freqs))]
    ; check that no sampled values lie outside support of the lnpdf
    (if (some (fn [c] (= 0 c)) (vals th-freqs))
      Double/POSITIVE_INFINITY
      (reduce + (map (fn [ec tc]
                       (let [dc (- ec tc)]
                         (/ (* dc dc) ec)))
                     (vals freqs)
                     (vals th-freqs))))))

(defn chisq-quantile
  [samples lnpdf]
  (let
    [freqs (frequencies samples)
     X (chisq-statistic freqs lnpdf)]
    ; evaluate cdf
    (. umontreal.iro.lecuyer.probdist.ChiSquareDist
       cdf
       ; degrees of freedom
       (dec (count freqs))
       ; decimal digits of precision
       10
       X)))

(defn ks-statistic [samples theoretical-cdf]
  "Calculates and empirical CDF of the samples and returns the KS-test statistic: sup_x |ECDF(x) - TCDF(x)|"
  (let
    [N (count samples)
     freqs (frequencies samples)
     emp-support (sort (keys freqs))
     emp-cdf (reduce (fn [ps x]
                       (let [p (or (peek ps) 0.0)]
                         (conj ps (+ p (/ (freqs x) N)))))
                     []
                     emp-support)
     th-cdf (map theoretical-cdf emp-support)]
    (reduce max (map (fn [ep tp] (abs (- ep tp))) emp-cdf th-cdf))))

(defn ks-quantile
  ([samples theoretical-cdf eff-sample-size]
   (let
     [ks-stat (ks-statistic samples theoretical-cdf)]
     (. umontreal.iro.lecuyer.probdist.KolmogorovSmirnovDist
        cdf
        eff-sample-size
        ks-stat)))
  ([samples theoretical-cdf]
   (ks-quantile samples theoretical-cdf (count samples))))
