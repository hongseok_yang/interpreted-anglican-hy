(defproject anglican "0.1.0-SNAPSHOT"
  :description "Anglican probabilistic program interpreter"
  :url "http://www.robots.ox.ac.uk/~fwood/anglican"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :repl-options {:nrepl-middleware [anglican.nrepl/middleware]
  				 :port 65432}
  :plugins [[lein-expectations "0.0.8"] [lein-ritz "0.7.0"] [test2junit "1.1.0"]]
  :jvm-opts ["-Djava.awt.headless=true"]
  :dev-dependencies [[lein-expectations "0.0.8"]
                     [expectations "1.4.16"]]
  :repositories {"local" "file:maven"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/tools.cli "0.2.1"]
                 [org.blancas/kern "0.7.0"]
                 [slingshot "0.10.3"]
                 [incanter "1.5.4"]
                 [colt "1.2.0"]
                 [org.codehaus.jsr166-mirror/jsr166y "1.7.0"]
                 [com.taoensso/timbre "3.1.6"]
                 [org.clojure/data.csv "0.1.2"]
                 [umontreal-ssj "2.5.0"]
                 [forssj-optimization "1.0.0"]
                 [org.clojure/data.json "0.2.5"]
                 [net.mikera/core.matrix "0.28.0"]
                 [net.mikera/vectorz-clj "0.24.0"]
                 [org.clojure/data.priority-map "0.0.5"]
                 [org.clojure/algo.monads "0.1.5"]
                 ;[org.clojure/algo.generic "0.1.2"]
                 ;[criterium "0.4.3"]
                 ;[org.clojure/tools.trace "0.7.5"]
                 [org.clojure/tools.nrepl "0.2.3"]
                 ;[clatrix "0.3.0"]
                 ;[org.jblas/jblas "1.2.2"]
                 ]
  :main anglican.main)
