# Anglican Language Overview

## Introduction 

Anglican is a research probabilistic programming language that demonstrates a
new approach to inference in expressive probabilistic programming languages
based on particle Markov chain Monte Carlo. Anglican is higher-order,
Turing-complete, and supports accurate inference in models that make use of
complex control flow, including stochastic recursion. It also includes
primitives from Bayesian nonparametric statistics.

## Language Basics

### Notation

Anglican is an intrepreted language that is implemented in Clojure. From here
onward we will distinguish between operations in the host language (Clojure)
and the guest language (Anglican). The operator `=>` will signify the result of
evaluation in Anglican, e.g.

    expr => value

The operator `->` signifies the result of an evaluation in Clojure, e.g.

    (eval state expr) -> [state value]

We will additionally use `<->` and `<=>` to denote equivalence of expressions.

### Literals

**boolean:** true | false

**long:** 1 | -1

**rational:** 1/2 | -1/2

**double:** 1.0 | -1.0 | 1. | -1. | .1 | -.1

**string:** "hello world"

**nil:** nil 


### Core Procedures

**(eval 'expr)**

Evaluates a quoted expression

    (eval 'expr) <=> expr => value

In the Clojure host language, this operation expands to

    (eval state '(eval 'expr)) <-> (eval state 'expr) -> [state value]

---

**(apply proc-expr arg-list)**

Applies a procedure to the sequence of arguments `arg-list`:

    (apply proc-arg (list arg-1 arg-2 ...)) <=> (proc-arg arg-1 arg-2 ...) => value

This operation equivalent to the following statements in the Clojure host language

    (eval state '(apply proc-expr arg-list))
        <-> (apply state (eval state proc-expr) (eval-seq arg-list))
         -> [state value]

Here `eval-seq` sequentially evaluated the arguments, propagating any updates to interpreter state.

---

**(mem proc-expr)**

Constructs a `memoized` procedure instance from an expression `proc-expr` that must evaluate to a procedure. A memoized procedure instance has a unique `address`, which is used to store the outcome of each procedure call in the execution `state`. If a procedure call is made with a previously used set of arguments, a cached value is returned from the execution state.


### Special Forms

**(quote expr), 'expr**

A quoted expression. Yields an unevaluated expression.

    'expr <=> (quote expr) => expr

---

**(define symbol expr)**

Extends the current environment by binding `symbol` to `expr`.

    (define symbol expr) => symbol

This operation expands to

    (eval state '(define symbol expr)) -> [(def-var state symbol (eval state expr)) symbol]

---

**(lambda (symbols) exprs), (lambda symbol exprs)**

Literal for an (anonymous) compound procedure.

    (lambda (symbol ...)  expr ...) => compound
    (lambda symbol expr ...) => compound

The function arguments are specified either as a list of symbols `(symbol ...)`,
or as a single symbol `symbol`, which holds the sequence of arguments in the
function body (allowing operations on argument lists of variable size).

A function body consists of one or more expressions. Upon application, the
compound procedure evaluates each `expr` in order (updating the
state if there are side effects) and returns the `value` associated with the
last `expr`. If there are no expressions, the function returns `nil`.

*Examples*

```
    ((lambda (n m)
       (* (+ n 1) m))
      1 2)
    => 4
```

```
    ((lambda (n m)
       (print "hello world")
       (* (+ n 1) m))
      1 2)
    => 4
```

```
    ((lambda values
       (sum values))
      1 2 3)
    => 6
```

```
    ((lambda (n m)))
    => nil
```

---

**(if bool-expr cons-expr alt-expr)**

Either evaluates `cons-expr` or `alt-expr`, depending on whether `bool-expr` evaluates to `true` or `false`. The `alt-expr` may be ommited in which case it is set to `nil`.

*Examples*

```
    (if (= 1 1)
        "the predicate is true"
        "the predicate is false")
    =>
    "the predicate is true"
```
```
    (if (= 1 2)
        "the predicate is true"
        "the predicate is false")
    =>
    "the predicate is false"
```

```
    (if (= 1 2)
        "the predicate is true")
    =>
    nil
```

---

**(cond (pred-expr cons-expr) ... (else alt-expr))**

Conditional statement. Evaluates each `pred-expr` and then evaluates the  first `cons-expr` where the predicate is `true`. If none of the predicates evaluate to `true`, then `alt-expr` is evaluated. This statement expands to a nested set of `if` statements, e.g.

    (cond (pred-1 cons-1)
          (pred-2 cons-2)
          (else alt))
    <=>
    (if pred-1
        cons-1
        (if pred-2
            cons-2
            alt))

---

**(begin exprs)**

Evaluates each `expr` in `exprs` in sequence, propagating updates to interpreter state, and returns the `value` of the last expression.

---

**(let ((symb expr) ...) exprs)**

Takes a list of bindings `(symb expr)` and calls `(define symb expr)` 
for each binding, then evaluates each `expr` in `exprs` in sequence, 
returning the `value` associated with the last evaluation.

A let statement 

    (let ((a 1) 
          (b 2))
      (prn "hello world")
      (+ a b))

is equivalent to the code

    (begin
      (define a 1)
      (define b 2)
      (prn "hello world")
      (+ a b))

bindings inside a `let` statement may locally override bindings in 
the enclosing environment.

**(recur expr)**

Anglican has no mechanism for tail-call optimization. The special form `recur` is used to explicitly label a recursive function call for which the current stack frame may be discarded, allowing non-stack consuming loops in a manner analogous to those provided by [Clojure](http://clojure.org/special_forms#recur). Warning: Anglican current does not explicity validate the usage of `recur` (which may only be used in the tail call position of a compound procedure).

---

**(system symbol ...)**

Queries internal system variables. At the time of writing supported calls are

    (system calls eval) => long       # number of eval calls made
    (system calls apply) => long      # number of apply calls made
    (system elapsed-time) => long     # nanoseconds since last timer respectively

## Language Syntax

In the following grammar rules, white space is omitted for brevity, and should
be present whenever needed to resolve ambiguity.

	program = directive { directive } 
	directive =  { assume | observe | predict | observe-csv  | import } 

### Directives

	assume = "[" "assume" variable expression "]" 
	observe = "[" "observe" distribution expression "]" 
	predict = "[" "predict" expression "]" 
	observe-csv = "[" "observe-csv" url csv-distribution csv-expression "]" 

### Expressions

	expression = special-form | application | variable | literal
	special-form = lambda | let | define | if | cond | begin
	application = "(" expression {expression} ")"

### Special Forms
	
	lambda = "(" "lambda" parameters expresion { expresion } ")" 
		parameters = "(" { variable } ")" | variable 
    let = "(" "let" "(" { binding } ")" experssion { experssion } ")"
		binding = "(" variable experssion ")"
	define = "(" "define" variable expression ")"
	if = "(" "if" expression expression [ expression ] ")"
	cond = "(" "cond" cond-clause { cond-clause } [ else-clause ] ")"
		cond-clause = "(" expression expression ")"
		else-clause = "(" "else" expression ")"
	begin = "(" "begin" expression { expression } ")"

### Literals

	literal = self-evaluating | quotation 
	quotation = "(" "quote" datum ")" | "'" datum
	datum = self-evaluating | symbol | list
	list = "(" { datum } ")"
	self-evaluating = boolean | long | rational | double | string
	boolean = "true" | "false"

In `symbol`, `long`, `rational`, `double`, `string`, spaces are not allowed
except when explicitly specified.

	long = ["+"|"-"] digit {digit}
	rational = integer "/" integer
	double = floating-point | floating-point exponent | integer exponent
		floating-point = ["+"|"-"] digit {digit} "." {digit} | {digit} "." digit {digit} 
		exponent = "E" integer
    string = "\"" {character} "\""
	symbol = symbol-initial-character {symbol-character}
	symbol-initial-character = "*" | "+" | "!" | "_" | "?" | "a" .. "z" | "A" .. "Z"
	symbol-character = symbol-initial-character | "-" | "0" .. "9"

## Core Library

### Primitives

Anglican primitives are procedures that are implemented in Clojure. As a consequence, primitives cannot mutate the execution state. The current list of primitives is

*tests:* **nil?, some?, empty?, seq?, list?, symbol?, string?, boolean?, proc?, number?, ratio?, integer?, float?,  isfinite?, isnan?, even?, odd?**

*relational:*  **and, or, not=, =, >, >=, <, <=**

*casting:*  **long, double, boolean, str, read-string**

*sequences:* **list, first, second, nth, rest, count, conj, unique**

*arithmetic:* **+, -, *, /, log, log10, exp, pow, sqrt, cbrt, floor, ceil, round, rint, abs, signum, sin, cos, tan, asin, acos, atan, sinh, cosh, tanh, inc, dec, mod, sum, cumsum, mean, normalize**

*io:* **prn**

### Elementary Random Procedures

Elementary random procedures (ERPs) are functions that satisfy two requirements

1. An ERP application in Anglican `(erp arg ...) => value` returns random values that are independent and identically distributed (i.i.d.) conditioned on the arguments to the procedure. ERP applications do not mutate the execution `state`.

2. For any `value` sampled from an ERP, the inference engine in Clojure must be able to calculate a log probability by calling `(score state erp args val) -> log-prob`.

---

**(beta a b)** sample from a Beta distribution with pseudocounts `a` and `b`. Returns a `double` on interval `[0,1)`.

**(binomial p n)** sample from a Binomial distribution with success
probability `p` and number of trials `n`. Returns an `long` on the interval `[0 ... n]`.

**(categorical ((val-1 p-1) ... (val-K) (p-K)))** sample from a categorical distribution parameterized by a sequence of pairs `(val p)`. Returns `val-k` with probability `(/ p-k (+ p-1 ... p-K))`.

**(dirichlet (alpha-1 ... alpha-K))** sample from a Dirichlet distribution parameterized by a sequence of pseudocounts `alpha`. Returns a sequence of probabilities `prob` such that `(sum prob) = 1.0` and `(count prob) = (count alpha)`.

**(discrete p)** sample from a discrete distribution parameterized by a sequence probabilities `p`. Returns an `long` in the range `[0 ... K-1]`, where `K = (count p)`  that takes `k` with probability `(/ (nth p k) (sum p))`.

**(exponential l)** sample from an exponential distribution with
with rate parameter `l`. Returns a double in the domain `[0, Inf)`.

**(flip p)** sample a single binomial trial. Returns `true` with probability `p` and `false` with probability `1-p`.

**(gamma a b)** samples from a Gamma distribution with shape `a` and rate `b`. Returns a `double` on the domain `(0, Inf)`.

**(invgamma a b)** samples from an inverse Gamma distribution with shape `a` and rate `b`. Returns a `double` on the domain `(0, Inf)`.

**(normal m s)** samples from a univariate normal distribution with mean `m` and standard deviation `s`. Returns a double on the domain `(-Inf, Inf)`

**(poisson l)** samples from a Poisson distribution with rate `l`.

**(uniform-continuous min max)** samples from a uniform continuous distribution. Returns a `double` in the domain `[min, max]`.

**(uniform-discrete min max)** samples from a uniform discrete distribution. Returns an `long` from the range `[min ... max-1]`.


### Exchangeable Random Procedures

Exchangeable random procedures (XRPs) satisfy three requirements

1. Application of an XRP yields a samples that are exchangeable, i.e. the joint
probability of a (possibly infinite) sequence of samples `(x ...)` must be
invariant under any (finite) permutation of `(x ...)`.

2. Sampling from XRPs can mutate the executation state, which typically holds a
set of sufficient statistics specific to each XRP instance. The Clojure
implementation of an XRP must provide procedures to `incorporate` and
`dissociate` a sampled `value` from the XRP statistics stored in the execution
`state`.

3. As with ERPs the Clojure implementation must provide a procedure to
calculate the log probability of a sample given the arguments and current
program state `(score state xrp args val) -> log-prob`.

Unlike ERPs, which are a form of primitive procedure, and XRP instance must be
initialized via a call to a primitive procedure `(xrp-init arg ...) => xrp`. The
resulting XRP instance `xrp` may then be called without arguments to obtain a
sample `(xrp) => value`.

---

**(crp alpha)** instantiates a Chinese Restaurant process `P` with
concentration parameter `alpha`. Sampling `(P)` conditioned on a set of
previous sample counts `((1 n-1) ... (K n-K))` yields an `long` that takes an
existing value `k` with probability `n-k / (+ alpha n-1 ... n-K)` and a new
value `K+1` with probability `alpha / (+ alpha n-1 ... n-K)`.

**(flip-beta a b)** instantiates posterior predictive distribution for a conjugate `flip` likelihood and `beta` prior. 

**(discrete-dirichlet alpha)** instantiates posterior predictive distribution for a conjugate `discrete` likelihood and `dirichlet` prior. 

## Evaluation of Anglican Programs
	
### Host-level Interpreter Protocol

Data structures in Anglican are *immutable*, in the sense that variables cannot be
modified after assignment. Unlike the host language Clojure, Anglican does not
provide mutable data structures (i.e. `atom` and `ref`) or dynamic variables
that can be altered via a `binding`. While Anglican is a purely *functional*
language in the sense it does not expose mechanisms that allow direct
manipulation of state, some functions (i.e. memoized and exchangeable random
procedures) do have side effects.

Interpretation of code in the host language (Clojure) is Monadic in the sense
that evaluation and function applications take the interpreter state as an
argument and return an (possibly updated) interpreter state, along with the
return value. The Anglican interpreter protocal defines 4 methods

```
(eval state expr) -> [state value]
(observe state expr value) -> [state sample]
(apply state proc args) -> [state value]
(score state proc args value) -> [state log-prob]
```

Here `expr` and `value` refer to any valid anglican expression and value type
respectively (see below). A `sample` object is a record with entries `[:value
:log-prob :proc :args]`, that is used internally for inference procedures, but
is not exposed in the Anglican directly. Finally `log-prob` is a double that
represent the log-probability associated with a random procedure application.
